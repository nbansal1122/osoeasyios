//
//  FailViewController.h
//  Demo
//
//  Created by Martin Prabhu on 5/25/16.
//  Copyright © 2016 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <netdb.h>
#import "MRMSDevFPiOS.h"
#import "PaymentModeViewController.h"

@interface FailViewController : UIViewController
{
    NSDictionary *dictPlist;
    NSString *CANCEL_VIEWCONTROLLER;
    
    
}
@property (nonatomic,retain)  NSString *session;

@property(nonatomic,retain)NSMutableDictionary *jsondict;
@property (strong, nonatomic) IBOutlet UILabel *lblthanks;

@property(nonatomic,retain)IBOutlet UIScrollView *scroll;
@property (strong, nonatomic) IBOutlet UITableView *tblview;

@end

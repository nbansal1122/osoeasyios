//
//  SucessViewTableViewCell.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 11/7/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SucessViewTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblValue;
@property (strong, nonatomic) IBOutlet UILabel *lblName;

@end

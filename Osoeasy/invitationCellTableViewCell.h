//
//  invitationCellTableViewCell.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/17/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface invitationCellTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *btnInvite;
@property (strong, nonatomic) IBOutlet UILabel *lblMobile;
@property (strong, nonatomic) IBOutlet UILabel *lblFlat;
@property (strong, nonatomic) IBOutlet UILabel *lblname;
@property (strong, nonatomic) IBOutlet UILabel *lblLast;

@end

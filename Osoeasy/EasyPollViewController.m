//
//  EasyPollViewController.m
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/14/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "EasyPollViewController.h"
#import "EventCellTableViewCell.h"
#import "ParkingTableViewCell.h"
#import "NoticeTableViewCell.h"
#import "ComplaintTableViewCell.h"
#import "invitationCellTableViewCell.h"
#import "AppDelegate.h"
#import "AlarmVC.h"
#import "PollResultsVC.h"
@interface EasyPollViewController ()
{
    NSMutableArray *arraydata;
    NSString * strmembrcode;
}
@end

@implementation EasyPollViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arraydata =[[NSMutableArray alloc] init];
    //  [AppDelegate creatingCustomButton:self.view];
    if ([_strType isEqualToString:@"invite"]) {
        [self getData];
    }
    else
    {
          [self getParkingResult];
    
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arraydata.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([_strType isEqualToString:@"EasyPoll"]) {
        EventCellTableViewCell*	cell=[tableView dequeueReusableCellWithIdentifier:@"newsCell"];
        
        if (cell==nil) {
            cell=[[[NSBundle mainBundle] loadNibNamed:@"EventCellTableViewCell" owner:self options:nil] objectAtIndex:0];
             [self addingShadowLayerOnView:cell.viewTop];
        }
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"Topic"] isKindOfClass:[NSNull class]]) {
            cell.lblTitle.text = [NSString stringWithFormat:@"  %@", [[arraydata objectAtIndex:indexPath.row] valueForKey:@"Topic"]];
            
        }
      
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"VoteCount"] isKindOfClass:[NSNull class]]) {
            cell.lblrate.text =[NSString stringWithFormat:@"%@", [[arraydata objectAtIndex:indexPath.row] valueForKey:@"VoteCount"]];
            
        }
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"CloseDate"] isKindOfClass:[NSNull class]]) {
            cell.lblEnddate.text =[NSString stringWithFormat:@"End: %@",[[arraydata objectAtIndex:indexPath.row] valueForKey:@"CloseDate"]];
            
        }
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"PublishDate"] isKindOfClass:[NSNull class]]) {
            cell.lblPublish.text =[NSString stringWithFormat:@"Published: %@",[[arraydata objectAtIndex:indexPath.row] valueForKey:@"PublishDate"]];
            
        }
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"Status"] isKindOfClass:[NSNull class]]) {
            [cell.btnClose setTitle:[NSString stringWithFormat:@"%@",[[arraydata objectAtIndex:indexPath.row] valueForKey:@"Status"]] forState:UIControlStateNormal];
            
        }
        tableView.backgroundColor =[UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        cell.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        self.view.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        cell.viewTop.backgroundColor = [UIColor whiteColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.viewTop.clipsToBounds=YES;
        cell.viewTop.layer.cornerRadius=4.0;
        
        return cell;
    }
    else if ([_strType isEqualToString:@"Notice"]) {
        NoticeTableViewCell*	cell=[tableView dequeueReusableCellWithIdentifier:@"newsCell"];
        
        if (cell==nil) {
            cell=[[[NSBundle mainBundle] loadNibNamed:@"NoticeTableViewCell" owner:self options:nil] objectAtIndex:0];
        }
        
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"nDetails"] isKindOfClass:[NSNull class]]) {
            NSString *trimmed = [[[arraydata objectAtIndex:indexPath.row] valueForKey:@"nDetails"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            trimmed = [trimmed stringByReplacingOccurrencesOfString:@"\n"
                                                 withString:@""];
            cell.lblDesc.text = trimmed ;
            
        }
       

         CGFloat temp =   [self getLabelHeight:cell.lblDesc];
       
              cell.lblDesc.frame = CGRectMake(  cell.lblDesc.frame.origin.x,   cell.lblTnoucment.frame.origin.y+cell.lblTnoucment.frame.size.height + 5,   cell.lblDesc.frame.size.width, temp +20);
        
        
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"nTitle"] isKindOfClass:[NSNull class]]) {
            cell.lblTitle.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"nTitle"];
            
        }
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"nType"] isKindOfClass:[NSNull class]]) {
            cell.lblTnoucment.text =[NSString stringWithFormat:@"%@ %@",[[arraydata objectAtIndex:indexPath.row] valueForKey:@"nType"], [[arraydata objectAtIndex:indexPath.row] valueForKey:@"nDate"]];
            
        }
        cell.iimg.layer.cornerRadius = 35;
        cell.iimg.clipsToBounds = true;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        tableView.backgroundColor =[UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        cell.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        self.view.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.vwtop.clipsToBounds=YES;
        cell.vwtop.layer.cornerRadius=4.0;
        tableView.backgroundColor =[UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        cell.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        self.view.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [self m_ShadowWithView:cell.vwtop];
        return cell;
    }
    else if ([_strType isEqualToString:@"complaint"]) {
        ComplaintTableViewCell*	cell=[tableView dequeueReusableCellWithIdentifier:@"newsCell"];
        
        if (cell==nil) {
            cell=[[[NSBundle mainBundle] loadNibNamed:@"ComplaintTableViewCell" owner:self options:nil] objectAtIndex:0];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        tableView.backgroundColor =[UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        cell.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        self.view.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"TicketNo"] isKindOfClass:[NSNull class]]) {
            cell.lblTicket.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"TicketNo"];
            
        }
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"TicketDate"] isKindOfClass:[NSNull class]]) {
            cell.lblDate.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"TicketDate"];
            
        }
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"TypeCode"] isKindOfClass:[NSNull class]]) {
            cell.lblcomplaintnO.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"TypeCode"];
            
        }
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"MemberName"] isKindOfClass:[NSNull class]]) {
            cell.lblmembername.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"MemberName"];
            
        }
        tableView.backgroundColor =[UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        cell.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        self.view.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.vwtop.layer.cornerRadius = 4;
        [self m_ShadowWithView:cell.vwtop];

        return cell;
    }
    else if ([_strType isEqualToString:@"invite"]) {
        invitationCellTableViewCell*	cell=[tableView dequeueReusableCellWithIdentifier:@"newsCell"];
        
        if (cell==nil) {
            cell=[[[NSBundle mainBundle] loadNibNamed:@"invitationCellTableViewCell" owner:self options:nil] objectAtIndex:0];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        tableView.backgroundColor =[UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        cell.btnInvite.tag = indexPath.row;
        [cell.btnInvite addTarget: self
                  action: @selector(sendTicketbutton:)
        forControlEvents: UIControlEventTouchUpInside];
        cell.lblFlat.text = [NSString stringWithFormat:@"%@",[[arraydata objectAtIndex:indexPath.row] valueForKey:@"Flat Details"]];
        cell.lblLast.text = [NSString stringWithFormat:@"%@",[[arraydata objectAtIndex:indexPath.row] valueForKey:@"Member Name"]];
        cell.lblname.text = [NSString stringWithFormat:@"%@",[[arraydata objectAtIndex:indexPath.row] valueForKey:@"Member Code"]];
        cell.lblMobile.text = [NSString stringWithFormat:@"%@",[[arraydata objectAtIndex:indexPath.row] valueForKey:@"Mobile #"]];

        tableView.backgroundColor =[UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        cell.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        self.view.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

        
        return cell;
    }
    else
    {
        ParkingTableViewCell*	cell=[tableView dequeueReusableCellWithIdentifier:@"newsCell"];
        
        if (cell==nil) {
            cell=[[[NSBundle mainBundle] loadNibNamed:@"ParkingTableViewCell" owner:self options:nil] objectAtIndex:0];
        }
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"MemberCode"] isKindOfClass:[NSNull class]]) {
            cell.lblCode.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"MemberCode"];

        }
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"VehicleType"] isKindOfClass:[NSNull class]]) {
            cell.lblType.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"VehicleType"];
            
        }
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"ParkingAreaName"] isKindOfClass:[NSNull class]]) {
            cell.lblAtrea.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"ParkingAreaName"];
            
        }
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"RegistrationNo"] isKindOfClass:[NSNull class]]) {
            cell.lblregnno.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"RegistrationNo"];
            
        }   if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"AllotmentNo"] isKindOfClass:[NSNull class]]) {
            cell.lblAlotmentNo.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"AllotmentNo"];
            
        }   if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"MemberName"] isKindOfClass:[NSNull class]]) {
            cell.lblSocityName.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"MemberName"];
            
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        tableView.backgroundColor =[UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        cell.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        self.view.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        cell.vwtop.backgroundColor = [UIColor whiteColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.vwtop.clipsToBounds=YES;
        cell.vwtop.layer.cornerRadius=4.0;
        [self m_ShadowWithView:cell.vwtop];

        return cell;
    }
        
  }


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_strType isEqualToString:@"EasyPoll"]) {
        NSString *str = [NSString stringWithFormat:@"%@",[[arraydata objectAtIndex:indexPath.row] valueForKey:@"Status"]];
        if ([str isEqualToString:@"Closed"]) {
            UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Voting not allowed on a closed poll" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];

        }
        else
        {
        PollResultsVC *objEasyPollViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PollResultsVC"];
        objEasyPollViewController.strpoolid = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"Pollid"];
        [self.navigationController pushViewController:objEasyPollViewController animated:nil];        //[self performSegueWithIdentifier:@"pollResultsView" sender:nil];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
     if ([_strType isEqualToString:@"EasyPoll"]) {
     
         return 120;
     }
     else if ([_strType isEqualToString:@"Notice"]) {
         if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"nDetails"] isKindOfClass:[NSNull class]]) {
            _lbltitle.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"nDetails"];
           CGFloat temp =   [self getLabelHeight:_lbltitle];
             return 75 + temp;

         }
 else if ([_strType isEqualToString:@"invite"]) {
     return 150;
 }
         return 90 ;

     }
    else
    {
    return 160;
    }
}
- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)addingShadowLayerOnView:(UIView *)view{
    view.layer.shadowColor=[[UIColor lightGrayColor]CGColor];
    view.layer.shadowRadius = 2.0f;
    view.layer.shadowOpacity =0.4f;
    view.layer.shadowOffset = CGSizeZero;
    view.layer.masksToBounds = NO;
}

-(void)doAction:(id)sender{
    AlarmVC *objHomeVC=[self.storyboard instantiateViewControllerWithIdentifier:@"alarmView"];
    [self.navigationController pushViewController:objHomeVC animated:YES];
}
-(void)getParkingResult{
//    spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleBounce color:[UIColor whiteColor]];
//    
//    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.square = YES;
//    hud.mode = MBProgressHUDModeCustomView;
//    hud.customView = spinner;
//    hud.labelText = NSLocalizedString(@"Loading", @"Loading");
    
    [self startHudAnimating];
    
    NSString *strUrl;
    if ([_strType isEqualToString:@"EasyPoll"]) {
         strUrl = [NSString stringWithFormat:@"%@%@?token=%@&email=%@",BASE_URL,EASY_POLL_1,Token,[self getuseremail]];
    }
    else if ([_strType isEqualToString:@"Notice"]) {
         strUrl = [NSString stringWithFormat:@"%@%@?token=%@&email=%@",BASE_URL,NOTICE_BOARD,Token,[self getuseremail]];
    }
    else if ([_strType isEqualToString:@"complaint"]) {
        strUrl = [NSString stringWithFormat:@"%@%@?token=%@&email=%@",BASE_URL,COMPLAIN_STATUS,Token,[self getuseremail]];

    }
    else
    {
 strUrl = [NSString stringWithFormat:@"%@%@?token=%@&email=%@",BASE_URL,PARKING_ALLOTMENT,Token,[self getuseremail]];
    }

   
    AFHTTPRequestOperationManager *manager=[[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:strUrl]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",nil];
    
    [manager GET:[NSString stringWithFormat:@"%@",@""] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *str=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]];
        if (![str isEqualToString:@"success"]) {
            UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:[responseObject valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            arraydata = [responseObject valueForKeyPath:@"data.table1"];
            [_tblView reloadData];
        }
        [self stopHudAnimating];
        NSLog(@"%@",  responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        [self stopHudAnimating];
        
    }];
    
}


-(BOOL)onSuccess:(id)object forTaskCode:(NSString *)taskcode httpObject:(HttpObject *)objHttp{
    [super onSuccess:object forTaskCode:taskcode httpObject:objHttp];
    NSString *str=[NSString stringWithFormat:@"%@",[object valueForKey:@"message"]];
    if (![str isEqualToString:@"success"]) {
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:[object valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        if ([taskcode isEqualToString:@"sendinvite"]) {
            UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Ticket has been submitted" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            //_txtbrif.text =@"";
            //_txtDetail.text = @"";
        }
        arraydata = [object valueForKeyPath:@"data.table1"];
        [_tblView reloadData];
        
    }
    
    return YES;
}

-(void)onFailure:(HttpObject *)paramObject forTaskCode:(NSString *)taskCode{
    [super onFailure:paramObject forTaskCode:taskCode];
    NSLog(@"RT %@", taskCode);
}


-(void)getData{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:Token forKey:@"token"];
    [dict setObject:[self getuseremail] forKey:@"email"];
    
    HttpObject *http = [[HttpObject alloc] initWithUrl:[NSString stringWithFormat:@"%@InvitationMembers",BASE_URL] withTaskCode:@"inviteList"];
    http.dicParams = dict;
    http.strMethodType = @"get";
    [self executeTask:http];
    
    
}
-(void)sendTicketbutton:(UIButton*)sender{
   strmembrcode = [NSString stringWithFormat:@"%@",[[arraydata objectAtIndex:[sender tag]] valueForKey:@"Member Code"]];
    _strinviteDesc = [_strinviteDesc stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    _strInviteTitle = [_strInviteTitle stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    _strInviteDate = [_strInviteDate stringByReplacingOccurrencesOfString:@" " withString:@"%20"];

    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:Token forKey:@"token"];
    [dict setObject:[self getuseremail] forKey:@"email"];
    [dict setObject:_strInviteDate forKey:@"InviteDate"];
    [dict setObject:_strInviteTitle forKey:@"InvitationTitle"];
    [dict setObject:strmembrcode forKey:@"MemberCode"];
    [dict setObject:_strinviteDesc forKey:@"InvitationLetter"];

    HttpObject *http = [[HttpObject alloc] initWithUrl:[NSString stringWithFormat:@"%@InvitationEntry",BASE_URL] withTaskCode:@"sendinvite"];
    http.dicParams = dict;
    http.strMethodType = @"get";
    [self executeTask:http];
    
    
}
-(void)m_ShadowWithView:(UIView *)view{
    view.layer.masksToBounds = NO;
    // view.layer.cornerRadius = 8; // if you like rounded corners
    view.layer.shadowOffset = CGSizeMake(0,1);
    view.layer.shadowRadius = 1.0;
    view.layer.shadowOpacity = 0.5;
}
@end

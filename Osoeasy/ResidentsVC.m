//
//  ResidentsVC.m
//  Osoeasy
//
//  Created by Apple on 17/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "ResidentsVC.h"
#import "customCell.h"
@interface ResidentsVC ()<UIScrollViewDelegate,UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate>
{
    int selectedindex;
    
    NSString *strApivalue,*strchildUrl;
    NSMutableArray *arrdata,*arrdata1,*arrdata2,*arrdata3,*arrsortData,*arropenIndex;
    NSMutableArray *uniqueArray;
}
@property (nonatomic, strong) HMSegmentedControl *segmentedControl4;

@end

@implementation ResidentsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    arrdata =[[NSMutableArray alloc] init];
    arrdata1 =[[NSMutableArray alloc] init];
    arrdata2 =[[NSMutableArray alloc] init];
    arrdata3 =[[NSMutableArray alloc] init];
    arrsortData =[[NSMutableArray alloc] init];
    arropenIndex =[[NSMutableArray alloc] init];
    uniqueArray =[[NSMutableArray alloc] init];

   
    _secondTableVIew.frame=CGRectMake(_objScrollView.frame.size.width, _secondTableVIew.frame.origin.y, _secondTableVIew.frame.size.width, _secondTableVIew.frame.size.height);
    _thirdTableView.frame=CGRectMake(_objScrollView.frame.size.width*2, _secondTableVIew.frame.origin.y, _secondTableVIew.frame.size.width, _secondTableVIew.frame.size.height);
    _FourthTableView.frame=CGRectMake(_objScrollView.frame.size.width*3, _FourthTableView.frame.origin.y, _FourthTableView.frame.size.width, _FourthTableView.frame.size.height);
    [self HMsegmentcontrol];
    // Do any additional setup after loading the view.
}



-(void)HMsegmentcontrol
{
    CGFloat viewWidth = CGRectGetWidth(self.view.frame);
 CGFloat viewheight = CGRectGetHeight(self.view.frame);
    
    self.segmentedControl4 = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(5, 0, viewWidth-10, 40)];
    if ([_strClassType isEqualToString:@"Resident"]) {
        self.segmentedControl4.sectionTitles = @[@"FAMILY", @"TENANTS", @"SERVANT/DRIVER"];
    }
    else{
        NSDictionary *dic =[[NSUserDefaults standardUserDefaults] valueForKey:@"userdata"];
        if (![[dic valueForKeyPath:@"userprofile.role"] isEqualToString:@"EMPLOYEE"]) {
            self.segmentedControl4.sectionTitles = @[@"RESIDENT's LIST", @"COMMITTEE STAFF", @"VENDORS HELP"];

        }
        else
        {
            self.segmentedControl4.sectionTitles = @[@"RESIDENT's LIST", @"COMMITTEE STAFF", @"VENDORS HELP",@"Home Staff"];

        }
    }
    
    
    self.segmentedControl4.selectedSegmentIndex = 0;
    self.segmentedControl4.titleTextAttributes =  @{
                                                    NSFontAttributeName : [UIFont fontWithName:@"Avenir Light" size:12.0],
                                                    NSForegroundColorAttributeName : [[UIColor whiteColor]colorWithAlphaComponent:0.6f]
                                                    };
    self.segmentedControl4.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [[UIColor whiteColor]colorWithAlphaComponent:0.6f]};
    
    self.segmentedControl4.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;
    self.segmentedControl4.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    [_segmentedControl4 addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    self.segmentedControl4.tag = 3;
    [_viewSegment addSubview:self.segmentedControl4];
    __weak typeof(self) weakSelf = self;
    [self.segmentedControl4 setIndexChangeBlock:^(NSInteger index) {
      [weakSelf.objScrollView scrollRectToVisible:CGRectMake(viewWidth * index, 0, viewWidth, viewheight-50) animated:YES];
        if (index==0) {
              [weakSelf.tableView reloadData];
        }
       else if (index==1) {
            [weakSelf.secondTableVIew reloadData];
        }
       else if (index==2) {
           [weakSelf.thirdTableView reloadData];
       }
       else if (index==3) {
           [weakSelf.FourthTableView reloadData];
       }
    }];
    [self setApi:selectedindex];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld (via UIControlEventValueChanged)", (long)segmentedControl.selectedSegmentIndex);
   
    selectedindex=(int)segmentedControl.selectedSegmentIndex;
    [self setApi:selectedindex];
    [UIView animateWithDuration:0.5 animations:^ {
    // [self SetTableAndCollectionView:selectedindex];
   
    _objScrollView.contentOffset=CGPointMake(_objScrollView.frame.size.width*selectedindex, 0);
       //  _tableView.frame=CGRectMake(_objScrollView.frame.size.width*selectedindex, _tableView.frame.origin.y, _tableView.frame.size.width, _tableView.frame.size.height);
     //   [_tableView reloadData];
        
 }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRecognizer:)];
    recognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    recognizer.delegate = self;
    [self.objScrollView addGestureRecognizer:recognizer];
    
    UISwipeGestureRecognizer *recognizer1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRecognizer:)];
    recognizer1.direction = UISwipeGestureRecognizerDirectionRight;
    recognizer1.delegate = self;
    [self.objScrollView addGestureRecognizer:recognizer1];
}
-(void)setApi :(int) selecteindex
{
    NSString * strtaskcode;
    if ([_strClassType isEqualToString:@"Resident"]) {
        if (selecteindex == 0) {
            strApivalue = @"fmCfamilymembers";
            strchildUrl = @"FamilyList";
            [self getDatawithoutchild:@"FamilyList"];

        }
        else if (selecteindex == 1)
        {
            strchildUrl = @"Tenants";
            [self getDatawithoutchild:@"Tenants"];

        }
        else
        {
            strchildUrl = @"ServentDriver";
            [self getDatawithoutchild:@"Driver"];

        }
        
    }
    else
    {
        if (selecteindex == 0) {
            strApivalue = @"mbCmembers";
            strchildUrl = @"ResidentsList";
            [self getDatawithoutchild:@"ResidentList"];

        }
        else if (selecteindex == 1)
        {
            strchildUrl = @"CommitteeStaffs";
             [self getDatawithoutchild:@"staff"];
        }
        else if (selecteindex == 3)
        {
            strchildUrl = @"ServentDriver";
            [self getDatawithoutchild:@"homeStaff"];
        }
        else
        {
            strchildUrl = @"HelpList";
            [self getDatawithoutchild:@"help"];

        }
        
        
    }
}

- (void)swipeRecognizer:(UISwipeGestureRecognizer *)sender {
    UIScrollView *scrol=(UIScrollView*)[sender view];
    NSLog(@"%f",scrol.contentOffset.x/self.view.frame.size.width);
    
    if (sender.direction == UISwipeGestureRecognizerDirectionLeft)
    {
        if (scrol.contentOffset.x/self.view.frame.size.width==3)
        {  }
        else{
            if (selectedindex<2) {
                selectedindex=selectedindex+1;
            }
            
        }
    }
    else if (sender.direction == UISwipeGestureRecognizerDirectionRight)
    {
        if (scrol.contentOffset.x/self.view.frame.size.width==0)
        {  }
        else{
            if (selectedindex>0) {
                selectedindex=selectedindex-1;
            }
        }
    }
    [self setApi:selectedindex];

    [UIView animateWithDuration:0.7 delay:0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.segmentedControl4.selectedSegmentIndex=selectedindex;
                       //  [self SetTableAndCollectionView:selectedindex];
                         _objScrollView.contentOffset=CGPointMake(_objScrollView.frame.size.width*selectedindex, 0);
                       //  [_tableView reloadData];

     }
                     completion:^(BOOL finished){ }];

//    [UIView animateWithDuration:0.5 animations:^ {
//    }];
}


//-(void)SetTableAndCollectionView:(NSInteger)index
//{
//        _tableView.frame=CGRectMake(_objScrollView.frame.size.width*selectedindex, _tableView.frame.origin.y, _tableView.frame.size.width, _tableView.frame.size.height);
//        [_tableView reloadData];
//}

-(void)selectheader:(UIButton*)sender
{
    if ([arropenIndex containsObject:[NSString stringWithFormat:@"%d",[sender tag]]]) {
        [arropenIndex removeObject:[NSString stringWithFormat:@"%d",[sender tag]]];
    }
    else
    {
        [arropenIndex addObject:[NSString stringWithFormat:@"%d",[sender tag]]];

    }
    _tableView.reloadData;

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ( selectedindex==0 && ![_strClassType isEqualToString:@"Resident"]) {
        for (int i = 0; i<arrdata.count; i++) {
            NSString *strtemp = [NSString stringWithFormat:@"APARTMENT Wing : %@ H.No. : %@",[[arrdata valueForKey:@"WingNo"] objectAtIndex:i],[[arrdata valueForKey:@"FlatNo"] objectAtIndex:i]];
            if (![uniqueArray containsObject:strtemp]) {
                [uniqueArray addObject:strtemp];
            }
        }
      
        return uniqueArray.count;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (selectedindex==0) {
        if ( ![_strClassType isEqualToString:@"Resident"]) {

        if ([arropenIndex containsObject:[NSString stringWithFormat:@"%d",section]]) {
            arrsortData =    [[self makeheader:[uniqueArray objectAtIndex:section]] mutableCopy];
            return   arrsortData .count;

        }
        }
        else
            return arrdata.count;
        return 0;
        
    }
    else if (selectedindex ==1)
    {
        return   arrdata1 .count;
        
    }
    else
    {
        return   arrdata2 .count;
        
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(5,2,tableView.frame.size.width-10,42)];
    if (selectedindex != 0 ||[_strClassType isEqualToString:@"Resident"])  {
        headerView.hidden = true;
        headerView = [[UIView alloc] initWithFrame:CGRectMake(5,2,tableView.frame.size.width-10,0)];
    }
    else
    {
        
    headerView.backgroundColor=[UIColor colorWithRed:256/255.0 green:128/255.0 blue:0/255.0 alpha:1];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0,tableView.frame.size.width-20,35)];
  
    headerLabel.textAlignment = NSTextAlignmentLeft;
    headerLabel.text = [uniqueArray objectAtIndex:section];
    [headerView addSubview:headerLabel];
    headerLabel.textColor =[UIColor whiteColor];
    UIButton *btn =[[UIButton alloc] initWithFrame:headerView.frame];
    [btn addTarget:self action:@selector(selectheader:)     forControlEvents:UIControlEventTouchUpInside];
    btn.tag = section;
    [headerView addSubview:btn];

       UILabel *headerLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 41,tableView.frame.size.width,1)];
    headerLabel1.backgroundColor = [UIColor whiteColor];
    [headerView addSubview:headerLabel1];

    
        headerView.hidden = false;

    }
    
    
    return headerView;
    
}
- (CGFloat)tableView:(UITableView *)tableView
heightForHeaderInSection:(NSInteger)section;
{
     if (selectedindex == 0 && ![_strClassType isEqualToString:@"Resident"]) {
    return 42.0f;
     }
    return  0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if (selectedindex==0 && ![_strClassType isEqualToString:@"Resident"]) {
            return 180;
        
    }
    return 130;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=@"cell";
    customCell *cellObj= (customCell *)[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if(cellObj == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"customCell" owner:self options:nil];
        cellObj = [nib objectAtIndex:0];
    }
    tableView.backgroundColor =[UIColor colorWithRed:196/255.0 green:196/255.0 blue:196/255.0 alpha:1.0];
    cellObj.backgroundColor = [UIColor colorWithRed:196/255.0 green:196/255.0 blue:196/255.0 alpha:1.0];
    self.view.backgroundColor = [UIColor colorWithRed:196/255.0 green:196/255.0 blue:196/255.0 alpha:1.0];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    if (selectedindex==0) {
        if ([_strClassType isEqualToString:@"Resident"]) {
            arr =   arrdata;

        }
        else
      arr =   arrsortData;
        
      
        if (![_strClassType isEqualToString:@"Resident"]) {
            cellObj.lblTitle.hidden = true;
            cellObj.vwintrnl.frame = CGRectMake(cellObj.vwintrnl.frame.origin.x, 5, cellObj.vwintrnl.frame.size.width, cellObj.vwintrnl.frame.size.height);
        cellObj.lblFlatValue.text =[NSString stringWithFormat:@"%@",[[arr valueForKey:@"Type"] objectAtIndex:indexPath.row]];
        cellObj.lblEmailValue.text =[NSString stringWithFormat:@"%@",[[arr valueForKey:@"EmailId"] objectAtIndex:indexPath.row]];
        cellObj.lblMobileValue.text =[NSString stringWithFormat:@"%@",[[arr valueForKey:@"CellNo"] objectAtIndex:indexPath.row]];
            cellObj.lblTitle.text =[NSString stringWithFormat:@"   %@",[[arr valueForKey:@"WingNo"] objectAtIndex:indexPath.row]];
            cellObj.lblFlat.text =@"Status :";
            
            cellObj.lblMemberValue.text =[NSString stringWithFormat:@"%@",[[arr valueForKey:@"FamilyName"] objectAtIndex:indexPath.row]];
            cellObj.lblresidenceValue.text =[NSString stringWithFormat:@"%@",[[arr valueForKey:@"Residence"] objectAtIndex:indexPath.row]];
            cellObj.lblrelationshipValue.text =[NSString stringWithFormat:@"%@",[[arr valueForKey:@"RelationShip"] objectAtIndex:indexPath.row]];
            cellObj.lblrelationshipValue.hidden = false;
            cellObj.lblresidenceValue.hidden = false;
            cellObj.lblMemberValue.hidden = false;
            cellObj.lblrelationship.hidden = false;
            cellObj.lblFamily.hidden = false;
            cellObj.lblResidence.hidden = false;

        }
        else
        {
            cellObj.lblFlatValue.text =[NSString stringWithFormat:@"%@",[[arr valueForKey:@"FlatDetails"] objectAtIndex:indexPath.row]];
            cellObj.lblEmailValue.text =[NSString stringWithFormat:@"%@",[[arr valueForKey:@"EmailID"] objectAtIndex:indexPath.row]];
            cellObj.lblMobileValue.text =[NSString stringWithFormat:@"%@",[[arr valueForKey:@"RegMobileNo"] objectAtIndex:indexPath.row]];
            cellObj.lblTitle.text =[NSString stringWithFormat:@"   %@",[[arr valueForKey:@"FMemberName"] objectAtIndex:indexPath.row]];
        }
    }
    else if (selectedindex ==1)
    {
        arr =   arrdata1;
        if ([_strClassType isEqualToString:@"Resident"]) {
            cellObj.lblFlatValue.text =[NSString stringWithFormat:@"%@",[[arr valueForKey:@"FlatDetails"] objectAtIndex:indexPath.row]];
            cellObj.lblEmailValue.text =[NSString stringWithFormat:@"%@",[[arr valueForKey:@"EmailID"] objectAtIndex:indexPath.row]];
            cellObj.lblMobileValue.text =[NSString stringWithFormat:@"%@",[[arr valueForKey:@"RegMobileNo"] objectAtIndex:indexPath.row]];
            cellObj.lblTitle.text =[NSString stringWithFormat:@"   %@",[[arr valueForKey:@"TenantName"] objectAtIndex:indexPath.row]];
            cellObj.lblFlat.text =@"Name :";

        }
        else
        {
            cellObj.lblFlatValue.text =[NSString stringWithFormat:@"%@",[[arr valueForKey:@"Name"] objectAtIndex:indexPath.row]];
            cellObj.lblEmailValue.text =[NSString stringWithFormat:@"%@",[[arr valueForKey:@"EmailId"] objectAtIndex:indexPath.row]];
            cellObj.lblMobileValue.text =[NSString stringWithFormat:@"%@",[[arr valueForKey:@"PhoneNo"] objectAtIndex:indexPath.row]];
            cellObj.lblTitle.text =[NSString stringWithFormat:@"   %@",[[arr valueForKey:@"Type"] objectAtIndex:indexPath.row]];
        }

    }
    else
    {
        if (![_strClassType isEqualToString:@"Resident"]) {
        arr =   arrdata2;
        cellObj.lblFlatValue.text =[NSString stringWithFormat:@"%@",[[arr valueForKey:@"Description"] objectAtIndex:indexPath.row]];
        cellObj.lblEmailValue.text =[NSString stringWithFormat:@"%@",[[arr valueForKey:@"ContPerson"] objectAtIndex:indexPath.row]];
        cellObj.lblMobileValue.text =[NSString stringWithFormat:@"%@",[[arr valueForKey:@"PhoneNo"] objectAtIndex:indexPath.row]];
        cellObj.lblFlat.text =@"Description :";
        cellObj.lblEmail.text =@"Name :";
        cellObj.lblMobileNo.text =@"Mobile No:";
            cellObj.lblTitle.text =[NSString stringWithFormat:@"   %@",[[arr valueForKey:@"Type"] objectAtIndex:indexPath.row]];

        }
    }
   
    [cellObj setSelectionStyle:UITableViewCellSelectionStyleNone];
    cellObj.selectionStyle=UITableViewCellSelectionStyleNone;
    return cellObj;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}
-(void)getData :(NSString*)taskcode{

    
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:Token forKey:@"token"];
    [dict setObject:[self getuseremail] forKey:@"email"];
    
    [dict setObject:@"" forKey:@"cond"];

    [dict setObject:strApivalue forKey:@"tblname"];
    HttpObject *http = [[HttpObject alloc] initWithUrl:[NSString stringWithFormat:@"%@%@",BASE_URL,strchildUrl] withTaskCode:taskcode];
    http.dicParams = dict;
    http.strMethodType = @"get";
    [self executeTask:http];
    
    
 }
-(void)getDatawithoutchild :(NSString*)taskcode{
    
    
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:Token forKey:@"token"];
    [dict setObject:[self getuseremail] forKey:@"email"];
    HttpObject *http = [[HttpObject alloc] initWithUrl:[NSString stringWithFormat:@"%@%@",BASE_URL,strchildUrl] withTaskCode:taskcode];
    http.dicParams = dict;
    http.strMethodType = @"get";
    [self executeTask:http];
    
    
}


-(BOOL)onSuccess:(id)object forTaskCode:(NSString *)taskcode httpObject:(HttpObject *)objHttp{
    [super onSuccess:object forTaskCode:taskcode httpObject:objHttp];
    NSString *str=[NSString stringWithFormat:@"%@",[object valueForKey:@"message"]];
    arrdata =[[NSMutableArray alloc] init];
    arrdata1 =[[NSMutableArray alloc] init];
    arrdata2 =[[NSMutableArray alloc] init];

    if ([str isEqualToString:@"success"]) {

    if (selectedindex==0) {
       
            arrdata = [object valueForKeyPath:@"data.table1"];
            _tableView.reloadData;
            
    }
    else if (selectedindex ==1)
    {
        arrdata1 = [object valueForKeyPath:@"data.table1"];
        _secondTableVIew.reloadData;


    }
        else
        {
            arrdata2 = [object valueForKeyPath:@"data.table1"];
            [_thirdTableView reloadData];
        }
    }
    return YES;
}

-(void)onFailure:(HttpObject *)paramObject forTaskCode:(NSString *)taskCode{
    [super onFailure:paramObject forTaskCode:taskCode];
    NSLog(@"RT %@", taskCode);
}

-(NSMutableArray*)makeheader:(NSString *)name
{
           NSMutableArray *arrtemp =[[NSMutableArray alloc] init];
    for (int i = 0; i<arrdata.count; i++) {
        NSString *strtemp = [NSString stringWithFormat:@"APARTMENT Wing : %@ H.No. : %@",[[arrdata valueForKey:@"WingNo"] objectAtIndex:i],[[arrdata valueForKey:@"FlatNo"] objectAtIndex:i]];
        

        if ([strtemp isEqualToString:name]) {
            [arrtemp addObject:[arrdata objectAtIndex:i]] ;
        }
    }
    return arrtemp;
}
@end

//
//  ProfileViewController.m
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/14/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "ProfileViewController.h"
#import "InvitationTableViewCell.h"
#import "ComplaintTableViewCell.h"
#import "AddNewTicketViewController.h"
#import "InvitationViewController.h"
#import "WebViewController1.h"
#import "BillingInfoVC.h"
@interface ProfileViewController ()
{
    NSMutableArray *arrdata;
    NSString *strprofileUrl;
        NSMutableArray *arraydata;
    
}

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    arrdata= [[NSMutableArray alloc] initWithObjects:@"Member Code : ", @"Socity Id : ",@"Father Name : ",@"Id Proof : ",@"id Proof Number : ", @"Pay Mode : ",@"Pay Ammount : ",@"Renewal Date : ",nil];
    arraydata=[[NSMutableArray alloc] init];
   //  [AppDelegate creatingCustomButton:self.view];
    if ([_strType isEqualToString:@"profile"]) {
        
        [self.btnProfile setTitle:@"RENEW MY SUBSCRIPTION" forState:UIControlStateNormal];

    }
    else if ([_strType isEqualToString:@"complaint"])
    {
        [self.btnProfile setTitle:@"Create Ticket" forState:UIControlStateNormal];

    }
    else
    {
        [self.btnProfile setTitle:@"Create Invite" forState:UIControlStateNormal];

    }
    
    // Do any additional setup after loading the view from its nib.
//    [_tblView setAlwaysBounceVertical:NO];
//    [_tblView setScrollsToTop:NO];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [self getData];

}
#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([_strType isEqualToString:@"profile"]) {
        if (arraydata.count > 0) {
            return arrdata.count+1;

        }
        return 0;
    }
    else
    {
        return arraydata.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([_strType isEqualToString:@"profile"]) {
        
    
    if (indexPath.row == 0) {
        
        HeaderTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"newsCell"];
        if (cell==nil) {
            cell=[[[NSBundle mainBundle] loadNibNamed:@"HeaderTableViewCell" owner:self options:nil] objectAtIndex:0];
        }
        if (![[[arraydata objectAtIndex:0] valueForKey:@"MembersName"] isKindOfClass:[NSNull class]]) {
            cell.lblName.text = [[arraydata objectAtIndex:0] valueForKey:@"MembersName"];
            
        }
        cell.lblemail.text = [self getuseremail];
        dispatch_async(dispatch_get_global_queue(0,0), ^{
            NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:strprofileUrl]];
            if ( data == nil )
                return;
            dispatch_async(dispatch_get_main_queue(), ^{
                // WARNING: is the cell still using the same data by this point??
                cell.imgUser.image = [UIImage imageWithData: data];
            });
        });
      //  cell.imgUser.imageURL =[NSURL URLWithString:strprofileUrl];
        cell.imgUser.layer.cornerRadius = 50;
        cell.imgUser.layer.borderColor = [[UIColor whiteColor] CGColor];
        cell.imgUser.layer.borderWidth = 2;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;

    }
    else
    {
        ProfileTableViewCell*	cell=[tableView dequeueReusableCellWithIdentifier:@"newsCell"];
        
        if (cell==nil) {
            cell=[[[NSBundle mainBundle] loadNibNamed:@"ProfileTableViewCell" owner:self options:nil] objectAtIndex:0];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.lblHeading.text = [arrdata objectAtIndex:indexPath.row-1];
        switch (indexPath.row) {
            case 1:
                if (![[[arraydata objectAtIndex:0] valueForKey:@"MemberCode"] isKindOfClass:[NSNull class]]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[arraydata objectAtIndex:0] valueForKey:@"MemberCode"]];
                    
                }
                
                break;
            
            case 2:
                if (![[[arraydata objectAtIndex:0] valueForKey:@"SocietyId"] isKindOfClass:[NSNull class]]) {
                    cell.lblValue.text =[NSString stringWithFormat:@"%@", [[arraydata objectAtIndex:0] valueForKey:@"SocietyId"]];
                    
                }
                
                break;

            case 3:
                if (![[[arraydata objectAtIndex:0] valueForKey:@"FatherName"] isKindOfClass:[NSNull class]]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[arraydata objectAtIndex:0] valueForKey:@"FatherName"]];
                    
                }
                
                break;

            case 4:
                if (![[[arraydata objectAtIndex:0] valueForKey:@"idProof"] isKindOfClass:[NSNull class]]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[arraydata objectAtIndex:0] valueForKey:@"idProof"]];
                    
                }
                break;

                
            case 5:
                if (![[[arraydata objectAtIndex:0] valueForKey:@"idProofNo"] isKindOfClass:[NSNull class]]) {
                    cell.lblValue.text =[NSString stringWithFormat:@"%@", [[arraydata objectAtIndex:0] valueForKey:@"idProofNo"]];
                    
                }
                break;

                
            case 6:
                if (![[[arraydata objectAtIndex:0] valueForKey:@"PayMode"] isKindOfClass:[NSNull class]]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[arraydata objectAtIndex:0] valueForKey:@"PayMode"]];
                    
                }
                break;

                
            case 7:
                if (![[[arraydata objectAtIndex:0] valueForKey:@"PayAmount"] isKindOfClass:[NSNull class]]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[arraydata objectAtIndex:0] valueForKey:@"PayAmount"]];
                    
                }
                break;

            case 8:
                if (![[[arraydata objectAtIndex:0] valueForKey:@"RenewalDate"] isKindOfClass:[NSNull class]]) {
                    cell.lblValue.text = [NSString stringWithFormat:@"%@",[[arraydata objectAtIndex:0] valueForKey:@"RenewalDate"]];
                    
                }
                
                break;
            default:
                break;
        }
        tableView.backgroundColor =[UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        cell.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        self.view.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
    }
    else if ([_strType isEqualToString:@"complaint"]) {
        ComplaintTableViewCell*	cell=[tableView dequeueReusableCellWithIdentifier:@"newsCell"];
        
        if (cell==nil) {
            cell=[[[NSBundle mainBundle] loadNibNamed:@"ComplaintTableViewCell" owner:self options:nil] objectAtIndex:0];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        tableView.backgroundColor =[UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        cell.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        self.view.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"TicketNo"] isKindOfClass:[NSNull class]]) {
            cell.lblTicket.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"TicketNo"];
            
        }
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"TicketDate"] isKindOfClass:[NSNull class]]) {
            cell.lblDate.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"TicketDate"];
            
        }
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"TypeCode"] isKindOfClass:[NSNull class]]) {
            cell.lblcomplaintnO.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"TypeCode"];
            
        }
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"MemberName"] isKindOfClass:[NSNull class]]) {
            cell.lblmembername.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"MemberName"];
            
        }
        cell.vwtop.layer.cornerRadius = 4;
        tableView.backgroundColor =[UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        cell.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        self.view.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
    else
    {
        InvitationTableViewCell*	cell=[tableView dequeueReusableCellWithIdentifier:@"newsCell"];
        
        if (cell==nil) {
            cell=[[[NSBundle mainBundle] loadNibNamed:@"InvitationTableViewCell" owner:self options:nil] objectAtIndex:0];
        }
        if (arraydata.count > 0) {
           // cell.vwTop.layer.cornerRadius = 8;
            [self m_ShadowWithView:cell.vwTop];
           // cell.vwTop.layer.cornerRadius = 10;
            cell.lblTitle.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"in_title"];
            cell.lblname.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"in_fmbid"];
            cell.lblDate.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"lDate"];
        }
     

        tableView.backgroundColor =[UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        cell.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        self.view.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;

    }
   }

-(void)m_ShadowWithView:(UIView *)view{
    view.layer.masksToBounds = NO;
   // view.layer.cornerRadius = 8; // if you like rounded corners
    view.layer.shadowOffset = CGSizeMake(0,1);
    view.layer.shadowRadius = 1.0;
    view.layer.shadowOpacity = 0.5;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   }
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if ([_strType isEqualToString:@"profile"]) {
    if (indexPath.row == 0) {
        return 248;
    }
    else
    {
        return  40;
    }
    }
    else
    {
        return 120;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
}

-(void)doAction:(id)sender{
    AlarmVC *objHomeVC=[self.storyboard instantiateViewControllerWithIdentifier:@"alarmView"];
    [self.navigationController pushViewController:objHomeVC animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnPaymentAction:(id)sender {
    if ([_strType isEqualToString:@"profile"]) {
    
        BillingInfoVC *controller=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"BillingInfoVC"];

      //  WebViewController1* controller = [[WebViewController1 alloc] initWithNibName:@"WebViewController1" bundle:nil];
        int r = rand();
        controller.strpage=@"ccvenue";
        controller.accessCode = @"AVCS64DD72AH63SCHA";
        controller.merchantId = @"94038";
        controller.amount = [[arraydata objectAtIndex:0] valueForKey:@"PayAmount"];
//        controller.amount = @"1";
        controller.billing_city = [[arraydata objectAtIndex:0] valueForKey:@"City"];
        controller.billing_country = [[arraydata objectAtIndex:0] valueForKey:@"Country"];
        controller.billing_email = [[arraydata objectAtIndex:0] valueForKey:@"Email"];
        controller.billing_tel = [[arraydata objectAtIndex:0] valueForKey:@"PhoneNo"];
        
        controller.billing_name = [[arraydata objectAtIndex:0] valueForKey:@"Name"];
        controller.billing_state = [[arraydata objectAtIndex:0] valueForKey:@"State"];
        controller.billing_address = [[arraydata objectAtIndex:0] valueForKey:@"Address"];

        controller.currency = @"INR";
        controller.orderId = [NSString stringWithFormat:@"%d",r];
        controller.redirectUrl = @"http://osoapi.azurewebsites.net/api/CCAvenuerRSASuccess/CCSucess";
        controller.cancelUrl = @"http://osoapi.azurewebsites.net/api/CCAvenuerRSACancel";
        controller.rsaKeyUrl = @"http://osoapi.azurewebsites.net/api/CCAvenuerRSA";
        
//        controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self.navigationController pushViewController:controller animated:YES];


    }
    else if ([_strType isEqualToString:@"complaint"])
    {
        AddNewTicketViewController *objEasyPollViewController=[[AddNewTicketViewController alloc] initWithNibName:@"AddNewTicketViewController" bundle:nil];
        
        [self.navigationController pushViewController:objEasyPollViewController animated:true];
    }
    else
    {
        InvitationViewController *objEasyPollViewController=[[InvitationViewController alloc] initWithNibName:@"InvitationViewController" bundle:nil];
        
        [self.navigationController pushViewController:objEasyPollViewController animated:true];
    }

}
-(void)getData{
    //    spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleBounce color:[UIColor whiteColor]];
    //
    //    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.square = YES;
    //    hud.mode = MBProgressHUDModeCustomView;
    //    hud.customView = spinner;
    //    hud.labelText = NSLocalizedString(@"Loading", @"Loading");
    
    [self startHudAnimating];
    
    NSString *strUrl;
    if ([_strType isEqualToString:@"profile"]) {
        strUrl = [NSString stringWithFormat:@"%@%@?token=%@&email=%@",BASE_URL,USER_PROFILE,Token,[self getuseremail]];
    }
    else if ([_strType isEqualToString:@"complaint"])
    {
        strUrl = [NSString stringWithFormat:@"%@%@?token=%@&email=%@",BASE_URL,COMPLAIN_STATUS,Token,[self getuseremail]];
        
    }
    else
    {
        strUrl = [NSString stringWithFormat:@"%@%@?token=%@&email=%@",BASE_URL,InvitationList,Token,[self getuseremail]];
    }
    AFHTTPRequestOperationManager *manager=[[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:strUrl]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",nil];
    
    [manager GET:[NSString stringWithFormat:@"%@",@""] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *str=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]];
        if (![str isEqualToString:@"success"]) {
            UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:[responseObject valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            arraydata = [responseObject valueForKeyPath:@"data.table1"];
            if ([_strType isEqualToString:@"profile"]) {
                float value =  [[[arraydata objectAtIndex:0] valueForKey:@"PayAmount"] floatValue];
                if (value<=0.0) {
                    _btnProfile.hidden = true;
                }
                [self profileimage];
            }
            else
            {
            [_tblView reloadData];
                [self stopHudAnimating];

            }
        }
        NSLog(@"%@",  responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        [self stopHudAnimating];
        
    }];

    
      
}
-(void)profileimage{
    //    spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleBounce color:[UIColor whiteColor]];
    //
    //    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.square = YES;
    //    hud.mode = MBProgressHUDModeCustomView;
    //    hud.customView = spinner;
    //    hud.labelText = NSLocalizedString(@"Loading", @"Loading");
    
    
    NSString *strUrl;
          strUrl = [NSString stringWithFormat:@"%@%@?token=%@&email=%@",BASE_URL,PHOTO_PROFILE,Token,[self getuseremail]];
    
    AFHTTPRequestOperationManager *manager=[[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:strUrl]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",nil];
    
    [manager GET:[NSString stringWithFormat:@"%@",@""] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *str=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]];
        if (![str isEqualToString:@"success"]) {
            UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:[responseObject valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            NSString *strfolder = [NSString stringWithFormat:@"%@",[[responseObject valueForKeyPath:@"data.table1.FolderName"] objectAtIndex:0]];
             NSString *strimage = [NSString stringWithFormat:@"%@",[[responseObject valueForKeyPath:@"data.table1.MyImage"] objectAtIndex:0]];
            strprofileUrl =[NSString stringWithFormat:@"https://society.osoeasy.in/%@/%@",strfolder,strimage];
            [_tblView reloadData];
        }
        [self stopHudAnimating];
        NSLog(@"%@",  responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        [self stopHudAnimating];
        
    }];
    
    
    
}
@end

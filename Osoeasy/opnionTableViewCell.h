//
//  opnionTableViewCell.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 10/4/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface opnionTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *btnRadio;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@end

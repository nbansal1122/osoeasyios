//
//  EasyPollViewController.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/14/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
@interface EasyPollViewController : BaseVC
@property (strong, nonatomic) IBOutlet UITableView *tblView;
@property (strong,nonatomic) NSString *strType;
@property (strong, nonatomic) IBOutlet UILabel *lbltitle;
@property (strong,nonatomic) NSString *strInviteTitle;
@property (strong,nonatomic) NSString *strinviteDesc;
@property (strong,nonatomic) NSString *strInviteDate;

@end

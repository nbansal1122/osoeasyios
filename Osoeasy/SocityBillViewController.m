//
//  SocityBillViewController.m
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/14/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "SocityBillViewController.h"
#import "ProfileTableViewCell.h"
#import "AppDelegate.h"
#import "AlarmVC.h"
#import "SocityBillViewController.h"
#import "BillingInfoVC.h"
@interface SocityBillViewController ()
{
    NSMutableArray *arrdata,*arrvalue;
    NSArray *arrayValues;
    NSMutableArray *arraydata;

}
@end

@implementation SocityBillViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arraydata=[[NSMutableArray alloc] init];

     // [AppDelegate creatingCustomButton:self.view];
}
-(void)viewWillAppear:(BOOL)animated
{
    arrdata= [[NSMutableArray alloc] initWithObjects:@"Bill Number",@"Bill Period", @"Due Date",@"Unit ID",@"Unit Name",@"Wing id", @"Bill Amount",@"Total Ammount",nil];
    arrvalue =[[NSMutableArray alloc] init];
    arrayValues=[NSArray arrayWithObjects:@"BillNo",@"BillPeriod",@"DueDate",@"UnitId",@"UnitName",@"WingId" ,@"BillAmount",@"TotalAmount",nil];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];

    [self getData];
    [self getDataprofile];
    // Do any additional setup after loading the view from its nib.
    _tblView.scrollEnabled=NO;
    if ([_strshowmsg isEqualToString:@"yes"]) {
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Transaction has been done." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrdata.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
           ProfileTableViewCell*	cell=[tableView dequeueReusableCellWithIdentifier:@"newsCell"];
        
        if (cell==nil) {
            cell=[[[NSBundle mainBundle] loadNibNamed:@"ProfileTableViewCell" owner:self options:nil] objectAtIndex:0];
        }
    if (arrvalue.count>0) {
        
    
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.lblHeading.text = [arrdata objectAtIndex:indexPath.row];
    cell.lblValue.text=[NSString stringWithFormat:@"%@",[[arrvalue objectAtIndex:0] valueForKey:[arrayValues objectAtIndex: indexPath.row]]]  ;
    }
        return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
        return  40;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}



-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(BOOL)onSuccess:(id)object forTaskCode:(NSString *)taskcode httpObject:(HttpObject *)objHttp{
    [super onSuccess:object forTaskCode:taskcode httpObject:objHttp];
    NSString *str=[NSString stringWithFormat:@"%@",[object valueForKey:@"message"]];
    if (![str isEqualToString:@"success"]) {
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:[object valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        arrvalue = [object valueForKeyPath:@"data.table1"];
        if (arrdata.count > 0) {
            _btnMakepayment.enabled = true;
        }
        else
        {
            _btnMakepayment.enabled = false;

        }
        [_tblView reloadData];
    }
    
    return YES;
}

-(void)onFailure:(HttpObject *)paramObject forTaskCode:(NSString *)taskCode{
    [super onFailure:paramObject forTaskCode:taskCode];
    NSLog(@"RT %@", taskCode);
}


-(void)getData{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:Token forKey:@"token"];
    [dict setObject:[self getuseremail] forKey:@"email"];
   
    HttpObject *http = [[HttpObject alloc] initWithUrl:[NSString stringWithFormat:@"%@BillPaymentHDFC",BASE_URL] withTaskCode:@"calendar"];
    http.dicParams = dict;
    http.strMethodType = @"get";
    [self executeTask:http];
    
    
}


-(void)doAction:(id)sender{
    AlarmVC *objHomeVC=[self.storyboard instantiateViewControllerWithIdentifier:@"alarmView"];
    [self.navigationController pushViewController:objHomeVC animated:YES];
}
-(void)getDataprofile{
    //    spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleBounce color:[UIColor whiteColor]];
    //
    //    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.square = YES;
    //    hud.mode = MBProgressHUDModeCustomView;
    //    hud.customView = spinner;
    //    hud.labelText = NSLocalizedString(@"Loading", @"Loading");
    
    
    NSString *strUrl;
        strUrl = [NSString stringWithFormat:@"%@%@?token=%@&email=%@",BASE_URL,USER_PROFILE,Token,[self getuseremail]];
   
    AFHTTPRequestOperationManager *manager=[[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:strUrl]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",nil];
    
    [manager GET:[NSString stringWithFormat:@"%@",@""] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *str=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]];
        if (![str isEqualToString:@"success"]) {
            UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:[responseObject valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            arraydata = [responseObject valueForKeyPath:@"data.table1"];
            
                 }
        NSLog(@"%@",  responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        [self stopHudAnimating];
        
    }];
    
    
    
}
- (IBAction)makePaymentAction:(id)sender {
    if (arraydata.count>0 && arrvalue.count>0 ) {
        
    
    BillingInfoVC *controller=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"BillingInfoVC"];
    
    //  WebViewController1* controller = [[WebViewController1 alloc] initWithNibName:@"WebViewController1" bundle:nil];
    int r = rand();
    controller.strpage=@"hdfc";
    controller.accessCode = @"AVCS64DD72AH63SCHA";
    controller.merchantId = @"94038";
    controller.amount = [[arrvalue objectAtIndex:0] valueForKey:@"TotalAmount"];
    //        controller.amount = @"1";
    controller.billing_city = [[arraydata objectAtIndex:0] valueForKey:@"City"];
    controller.billing_country = [[arraydata objectAtIndex:0] valueForKey:@"Country"];
    controller.billing_email = [[arraydata objectAtIndex:0] valueForKey:@"Email"];
    controller.billing_tel = [[arraydata objectAtIndex:0] valueForKey:@"PhoneNo"];
    controller.billing_duedate = [[arrvalue objectAtIndex:0] valueForKey:@"DueDate"];

    controller.billing_name = [[arraydata objectAtIndex:0] valueForKey:@"Name"];
    controller.billing_state = [[arraydata objectAtIndex:0] valueForKey:@"State"];
    controller.billing_address = [[arraydata objectAtIndex:0] valueForKey:@"Address"];
    
    controller.currency = @"INR";
    controller.orderId = [NSString stringWithFormat:@"%d",r];
    controller.redirectUrl = @"http://osoapi.azurewebsites.net/api/CCAvenuerRSASuccess/CCSucess";
    controller.cancelUrl = @"http://osoapi.azurewebsites.net/api/CCAvenuerRSACancel";
    controller.rsaKeyUrl = @"http://osoapi.azurewebsites.net/api/CCAvenuerRSA";
    
    //        controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self.navigationController pushViewController:controller animated:YES];
    

    
    
    
    }
        
    

}
@end

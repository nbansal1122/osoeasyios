//
//  WebViewController.m
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 10/16/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "WebViewController1.h"
#import "CCTool.h"
#import "ProfileViewController.h"
#import "SucessViewTableViewCell.h"
@interface WebViewController1 ()
{
    NSArray*arraydata;
}
@end

@implementation WebViewController1
@synthesize rsaKeyUrl;@synthesize accessCode;@synthesize merchantId;@synthesize orderId;
@synthesize amount;@synthesize currency;@synthesize redirectUrl;@synthesize cancelUrl;
- (void)viewDidLoad {
    [super viewDidLoad];

    arraydata =[NSArray arrayWithObjects:@"Billinng Name:",@"Transaction Id:",@"Description:",@"Amount:",@"Detail of Apartment:",@"Merchant Reference:",@"Payment Id:",@"Mobile Number:",@"Email:", nil];
    _scrlvw.contentSize = CGSizeMake(0, 600);
    _lblEmail.text =[NSString stringWithFormat:@"%@  %@",_lblEmail.text,_billing_email];
    _lblOrder.text =[NSString stringWithFormat:@"%@  %@",_lblOrder.text,orderId];
    _lblAmount.text =[NSString stringWithFormat:@"%@  %@",_lblAmount.text,amount];
    _lblSocityName.text =[NSString stringWithFormat:@"%@  %@",_lblSocityName.text,_billingSocityName];

    _lblName.text =[NSString stringWithFormat:@"%@  %@",_lblName.text,_billing_name];
    _lblAddress.text =[NSString stringWithFormat:@"%@  %@",_lblAddress.text,_billing_address];
    _lblMobile.text =[NSString stringWithFormat:@"%@  %@",_lblMobile.text,_billing_tel];
    _lblCity.text =[NSString stringWithFormat:@"%@  %@,%@",_lblCity.text,_billing_city,_billing_state];
   // _lblSocityName.text =[NSString stringWithFormat:@"%@  %@",_lblSocityName.text,_bi];

    self.webview.delegate = self;
    
    //Getting RSA Key
    NSString *rsaKeyDataStr = [NSString stringWithFormat:@"access_code=%@&order_id=%@&merchant_id=%@",accessCode,orderId,merchantId];
    NSData *requestData = [NSData dataWithBytes: [rsaKeyDataStr UTF8String] length: [rsaKeyDataStr length]];
    NSMutableURLRequest *rsaRequest = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: [NSString stringWithFormat:@"%@?%@",rsaKeyUrl,rsaKeyDataStr]]];
    //[rsaRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [rsaRequest setHTTPMethod: @"GET"];
    //[rsaRequest setHTTPBody: requestData];
    NSData *rsaKeyData = [NSURLConnection sendSynchronousRequest: rsaRequest returningResponse: nil error: nil];
    NSDictionary *jsonObject=[NSJSONSerialization
                              JSONObjectWithData:rsaKeyData
                              options:NSJSONReadingMutableLeaves
                              error:nil];
     NSString *rsaKey = [[NSString alloc] initWithData:rsaKeyData encoding:NSASCIIStringEncoding];
    rsaKey = [jsonObject valueForKey:@"data"];
   //rsaKey = [rsaKey stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    rsaKey = [NSString stringWithFormat:@"-----BEGIN PUBLIC KEY-----\n%@\n-----END PUBLIC KEY-----\n",rsaKey];
  /*  rsaKey = @"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAynKro+BmtNKPohCYrjEfR5488w60NfBN\n0xz6igKaNOV0fmXZCaOGe5ocUabADbZ6PSVUcdUeIlBQpDasw0jtWkqbjYPmT5mE3WljSzvXeHa9\ncPjIhE00FmSMhI4vsBalhu6wbnbJHq9LR2AcF7IgwA+SuSgoJwoqGCeaErPbCL+Q1fqkCpRA0BpG\nnjbDAOAIHG3yxm3+jHWkUmh1GaO8L9bMihTGXXesGdEIhIL5KDP4/llVOPzKUoAyj0ryb0TuIzKu\nSZdqHUJpX8HxASJ1zzCsiQZlPSnNvdbYtndYfysha83lrHeK4QM4L4NPXoxFaiQjEATF0JZGIUzW\n8gzbHQIDAQAB";
*/
    NSLog(@"%@",rsaKey);
    
    //Encrypting Card Details
    NSString *myRequestString = [NSString stringWithFormat:@"amount=%@&currency=%@",amount,currency];
    CCTool *ccTool = [[CCTool alloc] init];
    NSString *encVal = [ccTool encryptRSA:myRequestString key:rsaKey];
    encVal = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                   (CFStringRef)encVal,
                                                                                   NULL,
                                                                                   (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                   kCFStringEncodingUTF8 ));
    
    //Preparing for a webview call
    NSString *urlAsString = [NSString stringWithFormat:@"https://secure.ccavenue.com/transaction/initTrans"];
    NSString *encryptedStr = [NSString stringWithFormat:@"merchant_id=%@&order_id=%@&redirect_url=%@&cancel_url=%@&enc_val=%@&access_code=%@&billing_tel=%@&billing_zip=%@&billing_city=%@&billing_email=%@&billing_state=%@&billing_address=%@&billing_country=%@&billing_name=%@&delivery_name=%@&delivery_address=%@&delivery_city=%@&delivery_state=%@&delivery_zip=%@&delivery_country=India&delivery_tel=%@",merchantId,orderId,redirectUrl,cancelUrl,encVal,accessCode,_billing_tel,_billing_zip,_billing_city,_billing_email,_billing_state,_billing_address,@"India",_billing_name,_billing_name, _billing_address, _billing_city, _billing_state, _billing_zip,_billing_tel];
    
    NSData *myRequestData = [NSData dataWithBytes: [encryptedStr UTF8String] length: [encryptedStr length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlAsString]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setValue:urlAsString forHTTPHeaderField:@"Referer"];
    [request setHTTPMethod: @"POST"];
    [request setHTTPBody: myRequestData];
    [_webview loadRequest:request];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    NSString *string = webView.request.URL.absoluteString;
    NSLog(@"%@",string);
    if ([string rangeOfString:@"/CCAvenuerRSASuccess"].location != NSNotFound) {
        NSString *transStatus = @"Not Known";

        [self getData:@"success" amountval:@"1"];
        _lblStatus.text =[NSString stringWithFormat:@"%@  %@",_lblStatus.text,@"Sucess"];
        _sucessView.hidden =false;
        [_tableview reloadData];
        transStatus = @"Transaction Successful";
       /* NSString *html = [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.outerHTML"];
        
        NSString *transStatus = @"Not Known";
        
        if (([html rangeOfString:@"Aborted"].location != NSNotFound) ||
            ([html rangeOfString:@"Cancel"].location != NSNotFound)) {
            transStatus = @"Transaction Cancelled";
        }else if (([html rangeOfString:@"Success"].location != NSNotFound)) {
            [self getData:@"success" amountval:@"1"];

            transStatus = @"Transaction Successful";
        }else if (([html rangeOfString:@"Fail"].location != NSNotFound)) {
            [self getData:@"failure" amountval:@"1"];

            transStatus = @"Transaction Failed";
        }
        */
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:transStatus delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        //[alert show];

    /*    CCResultViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CCResultViewController"];
        controller.transStatus = transStatus;
        
        controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:controller animated:YES completion:nil];*/
    }
    else if ([string rangeOfString:@"/CCAvenuerRSACancel"].location != NSNotFound) {
    
        _lblStatus.text =[NSString stringWithFormat:@"%@  %@",_lblStatus.text,@"Failed"];
        _sucessView.hidden =false;
        [self getData:@"failure" amountval:@"1"];
_lblTransactionstatus.text = @"Your payment towards Socity Bill has been failed.";
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Transaction Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
       // [alert show];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)bckaction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
    //[self dismissViewControllerAnimated:YES completion:nil];

}

-(BOOL)onSuccess:(id)object forTaskCode:(NSString *)taskcode httpObject:(HttpObject *)objHttp{
    [super onSuccess:object forTaskCode:taskcode httpObject:objHttp];
    NSString *str=[NSString stringWithFormat:@"%@",[object valueForKey:@"message"]];
    if (![str isEqualToString:@"success"]) {
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:[object valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
       // [alert show];
    }
    else
    {
        
          }
    
    return YES;
}

-(void)onFailure:(HttpObject *)paramObject forTaskCode:(NSString *)taskCode{
    [super onFailure:paramObject forTaskCode:taskCode];
    NSLog(@"RT %@", taskCode);
}


-(void)getData :(NSString*)status amountval:(NSString*)amountval{
    //    spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleBounce color:[UIColor whiteColor]];
    //
    //    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.square = YES;
    //    hud.mode = MBProgressHUDModeCustomView;
    //    hud.customView = spinner;
    //    hud.labelText = NSLocalizedString(@"Loading", @"Loading");
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:Token forKey:@"token"];
    [dict setObject:[self getuseremail] forKey:@"email"];
    [dict setObject:status forKey:@"status"];
    [dict setObject:amountval forKey:@"amount"];

    HttpObject *http = [[HttpObject alloc] initWithUrl:[NSString stringWithFormat:@"%@CCAvenuerPayConfirm",BASE_URL] withTaskCode:@"CCAvenuerPayConfirm"];
    http.dicParams = dict;
    http.strMethodType = @"GET";
    [self executeTask:http];
    
    
   }

- (IBAction)profileACtrion:(id)sender {
    for (UIViewController* viewController in self.navigationController.viewControllers) {
        
        //This if condition checks whether the viewController's class is MyGroupViewController
        // if true that means its the MyGroupViewController (which has been pushed at some point)
        if ([viewController isKindOfClass:[ProfileViewController class]] ) {
            
            // Here viewController is a reference of UIViewController base class of MyGroupViewController
            // but viewController holds MyGroupViewController  object so we can type cast it here
            ProfileViewController *groupViewController = (ProfileViewController*)viewController;
            [self.navigationController popToViewController:groupViewController animated:YES];
        }
    }
//    ProfileViewController *obj=[self.storyboard instantiateViewControllerWithIdentifier:@"profileView"];
//    obj.strType =@"profile";
//    
//    self.sidePanelController.centerPanel = [[UINavigationController alloc]initWithRootViewController:obj];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
        return arraydata.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    

        SucessViewTableViewCell*	cell=[tableView dequeueReusableCellWithIdentifier:@"newsCell"];
    cell = nil;
        if (cell==nil) {
            cell=[[[NSBundle mainBundle] loadNibNamed:@"SucessViewTableViewCell" owner:self options:nil] objectAtIndex:0];
        }
    cell.lblName.text =[arraydata objectAtIndex:indexPath.row];
    switch (indexPath.row) {
        case 0:
            
            cell.lblValue.text = [NSString stringWithFormat:@"%@",_billing_name];
            
            
            
            break;
        case 1:
          
                cell.lblValue.text = [NSString stringWithFormat:@"%@",orderId];
                
            
            
            break;
            
        case 2:
    cell.lblValue.text = [NSString stringWithFormat:@"OSOeasy Socity Bill Payment"];

    
            break;
            
        case 3:
    cell.lblValue.text = [NSString stringWithFormat:@"%@",amount];
    
            break;
            
        case 4:
    cell.lblValue.text = [NSString stringWithFormat:@"%@",_billing_address];
  cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x, 0, self.view.bounds.size.width/2, 70);
            break;
            
            
        case 5:
    cell.lblValue.text = [NSString stringWithFormat:@"%@",merchantId];
            break;
            
            
        case 6:
    cell.lblValue.text = [NSString stringWithFormat:@"%@",orderId];

            break;
            
            
        case 7:
    cell.lblValue.text = [NSString stringWithFormat:@"%@",_billing_tel];
            break;
            
        case 8:
    cell.lblValue.text = [NSString stringWithFormat:@"%@",_billing_email];

    
            break;
        default:
            break;
    }

        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    

}

-(void)m_ShadowWithView:(UIView *)view{
    view.layer.masksToBounds = NO;
    // view.layer.cornerRadius = 8; // if you like rounded corners
    view.layer.shadowOffset = CGSizeMake(0,1);
    view.layer.shadowRadius = 1.0;
    view.layer.shadowOpacity = 0.5;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (indexPath.row ==4) {
        return 70;
    }
        return 40;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

@end

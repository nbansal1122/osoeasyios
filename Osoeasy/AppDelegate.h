//
//  AppDelegate.h
//  Osoeasy
//
//  Created by Apple on 12/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JASidePanelController.h"
#import "UIViewController+JASidePanel.h"
#import "AlarmVC.h"
@class JASidePanelController;
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) JASidePanelController *sidePanelObj;
+(void)creatingCustomButton:(UIView *)view;
@end


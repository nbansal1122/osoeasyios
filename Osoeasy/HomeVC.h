//
//  HomeVC.h
//  Osoeasy
//
//  Created by Apple on 13/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "BaseVC.h"
@interface HomeVC : BaseVC
@property (weak, nonatomic) IBOutlet UICollectionView *objCollectionView;
@property (strong, nonatomic) IBOutlet UITableView *tblvw;
@property (strong, nonatomic) IBOutlet UILabel *lbltitle;

@property (weak, nonatomic) IBOutlet UIButton *btnResponsiveTimeline;
@end

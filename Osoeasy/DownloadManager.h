//
//  DownloadManager.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/25/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HttpObject.h"
@protocol RestDelegate;
@interface DownloadManager : NSObject
-(instancetype)initWithHttpObject:(HttpObject*) objHttp Delegate:(id <RestDelegate>)restDelegate andtaskcode:(NSString *)taskCode;
@property(strong,nonatomic) HttpObject*obj;
@property(nonatomic,weak) id<RestDelegate> delegate;
@property(nonatomic,strong) NSString *strTaskCode;
-(void)startDownload;
@end
@protocol RestDelegate <NSObject>

@required
-(BOOL)onSuccess:(id)object forTaskCode:(NSString*)taskcode httpObject:(HttpObject*)objHttp;
-(void)onFailure:(id)paramObject forTaskCode:(NSString*)taskCode;
@optional
-(void)onPreExecute:(id)object forTaskCode:(NSString*)taskcode;
@end
//
//  SocityBillViewController.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/14/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"

#import <SystemConfiguration/SystemConfiguration.h>
#import <netdb.h>

#import "MRMSDevFPiOS.h"
#import "PaymentModeViewController.h"
@interface SocityBillViewController : BaseVC
@property (strong, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) IBOutlet UIButton *btnMakepayment;
- (IBAction)makePaymentAction:(id)sender;
@property (strong,nonatomic) NSString* strshowmsg;



- (IBAction)bckaction:(id)sender;
@property (strong, nonatomic) NSString *accessCode;
@property (strong, nonatomic) NSString *merchantId;
@property (strong, nonatomic) NSString *orderId;
@property (strong, nonatomic) NSString *amount;
@property (strong, nonatomic) NSString *currency;
@property (strong, nonatomic) NSString *redirectUrl;
@property (strong, nonatomic) NSString *cancelUrl;
@property (strong, nonatomic) NSString *rsaKeyUrl;



@property (strong, nonatomic) NSString *billingSocityName;

@property (strong, nonatomic) NSString *billing_name;
@property (strong, nonatomic) NSString *billing_address;
@property (strong, nonatomic) NSString *billing_email;
@property (strong, nonatomic) NSString *billing_tel;
@property (strong, nonatomic) NSString *billing_country;
@property (strong, nonatomic) NSString *billing_zip;
@property (strong, nonatomic) NSString *billing_state;
@property (strong, nonatomic) NSString *billing_city;

@property (strong, nonatomic) IBOutlet UILabel *lblEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblSocityName;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblCity;
@property (strong, nonatomic) IBOutlet UILabel *lblAmount;
@property (strong, nonatomic) IBOutlet UILabel *lblPayment;

@property (strong, nonatomic) IBOutlet UIView *sucessView;
@property (strong, nonatomic) IBOutlet UILabel *lblMobile;
- (IBAction)profileACtrion:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblOrder;
@property (strong, nonatomic) IBOutlet UILabel *lblStatus;

@property (strong, nonatomic) IBOutlet UILabel *lblRefresnce;
@end

//
//  ParkingTableViewCell.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/14/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParkingTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblCode;
@property (strong, nonatomic) IBOutlet UILabel *lblSocityName;
@property (strong, nonatomic) IBOutlet UILabel *lblType;
@property (strong, nonatomic) IBOutlet UILabel *lblAlotmentNo;
@property (strong, nonatomic) IBOutlet UILabel *lblAtrea;
@property (strong, nonatomic) IBOutlet UILabel *lblregnno;
@property (strong, nonatomic) IBOutlet UIView *vwtop;

@end

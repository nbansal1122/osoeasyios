//
//  AddMemberVC.h
//  Osoeasy
//
//  Created by Apple on 14/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddMemberVC : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *textFldMobileNumber;
@property (weak, nonatomic) IBOutlet UITextField *textFldRelationship;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmitOut;
@property (weak, nonatomic) IBOutlet UITextField *textFldName;
@end

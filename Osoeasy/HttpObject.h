//
//  HttpObject.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/25/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum : NSUInteger {
    DEFAULT = 0,
    SUCCESS,
    FAILED_UNKNOWN,
    FAILED_CONNECTION_ERROR,
    FAILED_SESSION_EXPIRED,
    FAILED_NO_INTERNET,
    FAILED_STATUS_FALSE,
    FAILED_PARSE_EXCEPTION,
    FAILED_NO_DATA_EXCEPTION,
} RtStatus;

@interface HttpObject : NSObject
- (instancetype)initWithUrl:(NSString *)pageUrl withTaskCode:(NSString *)taskCode;

@property (strong, nonatomic) NSString * strUrl;
@property (strong, nonatomic) NSString * strMethodType;
@property (strong, nonatomic) NSMutableDictionary * dicParams;
@property (strong, nonatomic) NSMutableDictionary * dicHeaders;
@property (strong, nonatomic) NSString * strtaskCode;

-(void)setMethod:(NSString *)method;
-(void)setTaskCodeValue:(NSString *)taskCodeValue;
-(void)addParam:(NSString *)key andValue:(NSString *)value;

@end

//
//  AddMemberVC.m
//  Osoeasy
//
//  Created by Apple on 14/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "AddMemberVC.h"

@interface AddMemberVC ()

@end

@implementation AddMemberVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSArray *arrText=[NSArray arrayWithObjects:@"Name",@"Mobile Number",@"Relationship",nil];
     NSArray *arrTextFld=[NSArray arrayWithObjects:_textFldName,_textFldMobileNumber,_textFldRelationship,nil];
    for (int i=0; i<arrText.count; i++) {
        [self setLabel:[arrText objectAtIndex:i] textFld:[arrTextFld objectAtIndex:i]];
        [[arrTextFld objectAtIndex:i]setRightViewMode:UITextFieldViewModeAlways];
    }
    
    for (UIView *view in self.view.subviews ) {
        [self addCALayerToTextFeild:view];
    }
    
    self.title=@"Add Member";
    UIBarButtonItem *btnback= [[UIBarButtonItem alloc]
                               initWithImage:[UIImage imageNamed:@"arrow"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonBackAddMember)];
    self.navigationItem.leftBarButtonItem = btnback;
    


    // Do any additional setup after loading the view.
}

-(void)buttonBackAddMember{
    [self.navigationController popViewControllerAnimated:YES];
  //  [self performSegueWithIdentifier:@"addMember" sender:nil];
}


-(void)addCALayerToTextFeild:(UIView*)view
{
    if ([view isKindOfClass:[UITextField class]]) {
        UITextField *textFld=(UITextField *)view;
        CALayer *layer = [CALayer layer];
        layer.borderColor = [UIColor blackColor].CGColor;
        layer.frame = CGRectMake(textFld.frame.origin.x, textFld.frame.origin.y+textFld.frame.size.height+1, textFld.frame.size.width,1);
        layer.borderWidth = 1;
        textFld.layer.masksToBounds = YES;
        [self.view.layer addSublayer:layer];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setLabel:(NSString *)text textFld:(UITextField *)textField
{
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(textField.frame),CGRectGetMinY(textField.frame),textField.frame.size.width,textField.frame.size.height)];
    label.text=text;
    label.tag=textField.tag+2;
    label.font=[UIFont systemFontOfSize:12.0];
    label.textColor=[UIColor lightGrayColor];
    [self.view addSubview:label];
}

#pragma mark --TextField Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField.text.length==0) {
        [self setAnimationViewImage:(UIImageView *)[self.view viewWithTag: textField.tag+1]   label:(UILabel *)[self.view viewWithTag:textField.tag+2]boolUpDown:true  textField:textField];
    }
}

-(void) textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField.text.length==0)
        [self setAnimationViewImage:(UIImageView *)[self.view viewWithTag:textField.tag+1] label:(UILabel *)[self.view viewWithTag:textField.tag+2]boolUpDown:false textField:textField];
}

-(void)setAnimationViewImage:(UIImageView *)imgView label:(UILabel *)labelFrame boolUpDown:(BOOL)boolUpDown textField:(UITextField *)textFld
{
    [UIView animateWithDuration:0.7 delay:0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         if (boolUpDown) {
                             imgView.frame = CGRectMake(imgView.frame.origin.x,textFld.frame.origin.y-15, imgView.frame.size.width, imgView.frame.size.height);
                             labelFrame.frame = CGRectMake(labelFrame.frame.origin.x,textFld.frame.origin.y-15, labelFrame.frame.size.width, labelFrame.frame.size.height);
                         }
                         else{
                             imgView.frame = CGRectMake(imgView.frame.origin.x,textFld.frame.origin.y, imgView.frame.size.width, imgView.frame.size.height);
                             labelFrame.frame = CGRectMake(labelFrame.frame.origin.x, textFld.frame.origin.y, labelFrame.frame.size.width, labelFrame.frame.size.height);
                         }
                     }
                     completion:^(BOOL finished){ }];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    [textField  resignFirstResponder];
    return YES;
}
@end

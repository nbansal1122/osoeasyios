//
//  PollResultsVC.m
//  Osoeasy
//
//  Created by Apple on 15/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "PollResultsVC.h"
#import "pollResultTableViewCell.h"
#import "opnionTableViewCell.h"
@interface PollResultsVC ()
{
    NSMutableArray *arrdata;
    NSInteger selectedindex;
}
@end

@implementation PollResultsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _vwopinion.hidden = true;
    arrdata =[[NSMutableArray alloc] init];
    [self getData];
    selectedindex = 0;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrdata.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == _tableview) {
        
   
    pollResultTableViewCell*	cell=[tableView dequeueReusableCellWithIdentifier:@"newsCell"];
    
    if (cell==nil) {
        cell=[[[NSBundle mainBundle] loadNibNamed:@"pollResultTableViewCell" owner:self options:nil] objectAtIndex:0];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.lbltitle.text = [[arrdata objectAtIndex:indexPath.row] valueForKey:@"Topic"];
    cell.vwProgress.barBorderColor = [UIColor lightGrayColor];
    cell.vwProgress.barFillColor = [UIColor orangeColor];
    [cell.vwProgress setBarBorderWidth:0.5];
    [cell.vwProgress setBarInnerPadding:0.0];
    [cell.vwProgress setUsesRoundedCorners:0.9];
    cell.vwProgress.layer.cornerRadius = 5;
    [cell.vwProgress setBarBackgroundColor:[UIColor clearColor]];
        cell.btnRadio.hidden = true;
    cell.vwProgress.progress = [[[arrdata valueForKey:@"AvgCounter"] objectAtIndex:indexPath.row] floatValue]/100.0;
    
    return cell;
     }
    else
    {
        
        pollResultTableViewCell*	cell=[tableView dequeueReusableCellWithIdentifier:@"newsCell"];
        
        if (cell==nil) {
            cell=[[[NSBundle mainBundle] loadNibNamed:@"pollResultTableViewCell" owner:self options:nil] objectAtIndex:0];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.vwProgress.hidden = true;
        cell.lbltitle.text = [[arrdata objectAtIndex:indexPath.row] valueForKey:@"Opinion"];
        cell.btnRadio.tag = indexPath.row;
        [cell.btnRadio addTarget: self
                       action: @selector(RadioAction:)
             forControlEvents: UIControlEventTouchUpInside];
        if (indexPath.row == selectedindex) {
            cell.btnRadio.selected = true;
        }
        else
        {
            cell.btnRadio.selected = false;

        }
        
        return cell;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tblopinion) {
        selectedindex = indexPath.row;
        [_tblopinion reloadData];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
     if (tableView == _tableview) {
    return  93;
     }
    return 60;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}
-(void)RadioAction:(UIButton*)sender
{
    selectedindex = sender.tag;
    sender.selected = !sender.selected;
    [_tblopinion reloadData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submitaction:(id)sender {
    _vwopinion.hidden = true;
    _strpoolid = [[arrdata objectAtIndex:[sender tag]] valueForKey:@"PollKey"];
    [self submitopinion];
}

- (IBAction)hideopinionview:(id)sender {
    _vwopinion.hidden = true;

}
- (IBAction)addopinionAction:(id)sender {
    _vwopinion.hidden = false;
}

-(BOOL)onSuccess:(id)object forTaskCode:(NSString *)taskcode httpObject:(HttpObject *)objHttp{
    [super onSuccess:object forTaskCode:taskcode httpObject:objHttp];
    NSString *str=[NSString stringWithFormat:@"%@",[object valueForKey:@"message"]];
    if (![str isEqualToString:@"success"]) {
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:[object valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        arrdata = [object valueForKeyPath:@"data.table1"];
        
        
    }
    [_tableview reloadData];
    [_tblopinion reloadData];
    return YES;
}

-(void)onFailure:(HttpObject *)paramObject forTaskCode:(NSString *)taskCode{
    [super onFailure:paramObject forTaskCode:taskCode];
    NSLog(@"RT %@", taskCode);
}


-(void)getData{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:Token forKey:@"token"];
    [dict setObject:[self getuseremail] forKey:@"email"];
    [dict setObject:_strpoolid forKey:@"PollId"];
    [dict setObject:@"" forKey:@"cond"];
    
    [dict setObject:@"caLcalendar" forKey:@"tblname"];
    HttpObject *http = [[HttpObject alloc] initWithUrl:[NSString stringWithFormat:@"%@OpinionPollResult",BASE_URL] withTaskCode:@"calendar"];
    http.dicParams = dict;
    http.strMethodType = @"get";
    [self executeTask:http];
    
    
}
-(void)submitopinion{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:Token forKey:@"token"];
    [dict setObject:[self getuseremail] forKey:@"email"];
    [dict setObject:_strpoolid forKey:@"PollId"];
    [dict setObject:@"" forKey:@"cond"];
    
    [dict setObject:@"caLcalendar" forKey:@"tblname"];
    HttpObject *http = [[HttpObject alloc] initWithUrl:[NSString stringWithFormat:@"%@OpinionPollResult",BASE_URL] withTaskCode:@"calendar"];
    http.dicParams = dict;
    http.strMethodType = @"get";
    [self executeTask:http];
    
    
}
@end

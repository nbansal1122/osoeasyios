//
//  MenuVC.m
//  Osoeasy
//
//  Created by Apple on 12/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "MenuVC.h"
#import "ProfileViewController.h"
#import "SocityBillViewController.h"
#import "EasyPollViewController.h"
#import "ViideoViewController.h"
#import "ResidentsVC.h"
#import "LoginViewController.h"

@interface MenuVC ()
{
    NSArray *arrayData,*arrayImages;
}
@end

@implementation MenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSDictionary *dic =[[NSUserDefaults standardUserDefaults] valueForKey:@"userdata"];
    if (![[dic valueForKeyPath:@"userprofile.role"] isEqualToString:@"EMPLOYEE"]) {
        arrayData=[NSArray arrayWithObjects:@"Dashboard",@"My Profile",  @"Society Bill", @"Calender",@"EasyPoll" ,@"Parking Allotment",@"Directory",@"Residents",@"Notice Board",@"Invitation",@"Photos",@"Videos",@"Complaints",@"Logout",nil];
        arrayImages=[NSArray arrayWithObjects:@"di_dashboard",@"di_residents",  @"di_residents", @"di_calender",@"di_easypoll" ,@"di_parking",@"di_directry",@"di_residents",@"di_noticeboard",@"di_invitation",@"di_communication",@"di_communication",@"di_complains",@"logout",nil];

    }
    else
    {
    
        arrayData=[NSArray arrayWithObjects:@"Dashboard",@"My Profile",  @"Society Bill", @"Calender" ,@"Parking Allotment",@"Directory",@"Residents",@"Notice Board",@"Invitation",@"Videos",@"Complaints",@"Logout",nil];
        arrayImages=[NSArray arrayWithObjects:@"di_dashboard",@"di_residents",  @"di_residents", @"di_calender" ,@"di_parking",@"di_directry",@"di_residents",@"di_noticeboard",@"di_invitation",@"di_communication",@"di_complains",@"logout",nil];
}
   
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayData.count;
}

- (float)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    // This will create a "invisible" footer
    return 0.01f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    cell.backgroundColor=[UIColor clearColor];
    cell.textLabel.textColor=[UIColor whiteColor];
    cell.textLabel.text =[arrayData objectAtIndex:indexPath.row];
    cell.imageView.image=[UIImage imageNamed:[arrayImages objectAtIndex:indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{


        if ([[arrayData objectAtIndex:indexPath.row]  isEqualToString:@"Dashboard"])
    {
        HomeVC *objHomeVC=[self.storyboard instantiateViewControllerWithIdentifier:@"homeView"];
        self.sidePanelController.centerPanel = [[UINavigationController alloc]initWithRootViewController:objHomeVC];
    }
    else  if ([[arrayData objectAtIndex:indexPath.row]  isEqualToString:@"My Profile"])

    {
       
          ProfileViewController *obj=[self.storyboard instantiateViewControllerWithIdentifier:@"profileView"];
        obj.strType =@"profile";

         self.sidePanelController.centerPanel = [[UINavigationController alloc]initWithRootViewController:obj];
    }
    else  if ([[arrayData objectAtIndex:indexPath.row]  isEqualToString:@"Society Bill"])
    {
        
          SocityBillViewController *obj=[self.storyboard instantiateViewControllerWithIdentifier:@"societyView"];
         self.sidePanelController.centerPanel = [[UINavigationController alloc]initWithRootViewController:obj];
    }
    else  if ([[arrayData objectAtIndex:indexPath.row]  isEqualToString:@"Calender"])
    {
        CalanderVC *objCalanderVC=[self.storyboard instantiateViewControllerWithIdentifier:@"calanderView"];
        self.sidePanelController.centerPanel = [[UINavigationController alloc]initWithRootViewController:objCalanderVC];
    }
    else  if ([[arrayData objectAtIndex:indexPath.row]  isEqualToString:@"EasyPoll"])
  {
      EasyPollViewController *objEasyPollViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"easyPollView"];
      objEasyPollViewController.strType = @"EasyPoll";
      self.sidePanelController.centerPanel = [[UINavigationController alloc]initWithRootViewController:objEasyPollViewController];


  }
    else  if ([[arrayData objectAtIndex:indexPath.row]  isEqualToString:@"Parking Allotment"])
  {
      EasyPollViewController *objEasyPollViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"easyPollView"];
      objEasyPollViewController.strType = @"Parking";
      self.sidePanelController.centerPanel = [[UINavigationController alloc]initWithRootViewController:objEasyPollViewController];
      
  }
    else  if ([[arrayData objectAtIndex:indexPath.row]  isEqualToString:@"Directory"])
  {
      
      ResidentsVC *objEasyPollViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"ResidentsVC"];
      objEasyPollViewController.strClassType = @"Directory";

      self.sidePanelController.centerPanel = [[UINavigationController alloc]initWithRootViewController:objEasyPollViewController];
      
  }
    else  if ([[arrayData objectAtIndex:indexPath.row]  isEqualToString:@"Residents"])
  {
      ResidentsVC *objEasyPollViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"ResidentsVC"];
      objEasyPollViewController.strClassType = @"Resident";

      self.sidePanelController.centerPanel = [[UINavigationController alloc]initWithRootViewController:objEasyPollViewController];
      
  }
    else  if ([[arrayData objectAtIndex:indexPath.row]  isEqualToString:@"Notice Board"])
  {
      
      EasyPollViewController *objEasyPollViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"easyPollView"];
      objEasyPollViewController.strType = @"Notice";
      self.sidePanelController.centerPanel = [[UINavigationController alloc]initWithRootViewController:objEasyPollViewController];
      
  }
    else  if ([[arrayData objectAtIndex:indexPath.row]  isEqualToString:@"Invitation"])
  {
      ProfileViewController *obj=[self.storyboard instantiateViewControllerWithIdentifier:@"profileView"];
      obj.strType =@"Invite";
      self.sidePanelController.centerPanel = [[UINavigationController alloc]initWithRootViewController:obj];
  }
    else  if ([[arrayData objectAtIndex:indexPath.row]  isEqualToString:@"Videos"])
  {
      ViideoViewController *obj=[[ViideoViewController alloc] initWithNibName:@"ViideoViewController" bundle:nil];
      obj.strtype =@"Videos";

self.sidePanelController.centerPanel = [[UINavigationController alloc]initWithRootViewController:obj];
  }
    else  if ([[arrayData objectAtIndex:indexPath.row]  isEqualToString:@"Photos"])
    {
        ViideoViewController *obj=[[ViideoViewController alloc] initWithNibName:@"ViideoViewController" bundle:nil];
        obj.strtype =@"Photos";
        self.sidePanelController.centerPanel = [[UINavigationController alloc]initWithRootViewController:obj];
    }
    else  if ([[arrayData objectAtIndex:indexPath.row]  isEqualToString:@"Complaints"])
  {
      
      
      ProfileViewController *obj=[self.storyboard instantiateViewControllerWithIdentifier:@"profileView"];
      obj.strType =@"complaint";
      self.sidePanelController.centerPanel = [[UINavigationController alloc]initWithRootViewController:obj];
     
  
  }
    else  if ([[arrayData objectAtIndex:indexPath.row]  isEqualToString:@"Logout"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert !" message:@"Are you sure want to logout" delegate:self cancelButtonTitle:@"No" otherButtonTitles: @"Yes",nil];
        [alert show];
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userdata"];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        LoginViewController *objLogin = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        
        UINavigationController *menuNavigationController = [[UINavigationController alloc] initWithRootViewController:objLogin];
        appDelegate.window.rootViewController = menuNavigationController;
    }
}

@end

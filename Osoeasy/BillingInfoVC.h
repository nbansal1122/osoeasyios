//
//  BillingInfoVC.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 11/2/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BillingInfoVC : UIViewController
- (IBAction)ProccedAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtzipcode;
@property (strong, nonatomic) IBOutlet UITextField *txtName;
- (IBAction)backAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtCity;
@property (strong, nonatomic) IBOutlet UITextField *txtMobileNumber;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtAddress;
@property (strong, nonatomic) IBOutlet UIScrollView *scrlvw;
@property (strong, nonatomic) IBOutlet UITextField *txtState;

@property (strong, nonatomic) IBOutlet UITextField *txtSocity;
@property (strong, nonatomic) IBOutlet UIButton *btnpayment;

@property (strong, nonatomic) NSString  *strpage;


@property (strong, nonatomic) NSString *billing_name;
@property (strong, nonatomic) NSString *billing_address;
@property (strong, nonatomic) NSString *billing_email;
@property (strong, nonatomic) NSString *billing_tel;
@property (strong, nonatomic) NSString *billing_country;
@property (strong, nonatomic) NSString *billing_zip;
@property (strong, nonatomic) NSString *billing_state;
@property (strong, nonatomic) NSString *billing_city;
@property (strong, nonatomic) NSString *billing_duedate;


@property (strong, nonatomic) NSString *accessCode;
@property (strong, nonatomic) NSString *merchantId;
@property (strong, nonatomic) NSString *orderId;
@property (strong, nonatomic) NSString *amount;
@property (strong, nonatomic) NSString *currency;
@property (strong, nonatomic) NSString *redirectUrl;
@property (strong, nonatomic) NSString *cancelUrl;
@property (strong, nonatomic) NSString *rsaKeyUrl;




@end

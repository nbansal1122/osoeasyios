//
//  calendarTableViewCell.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 10/4/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface calendarTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblDesc;

@end

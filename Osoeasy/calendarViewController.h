//
//  calendarViewController.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 10/4/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface calendarViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property(strong,nonatomic) NSArray *arrdata;
@end

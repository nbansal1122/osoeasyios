//
//  ViideoViewController.m
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/15/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "ViideoViewController.h"
#import "VideoCollectionViewCell.h"
@interface ViideoViewController ()
{
    NSMutableArray *arrdata ;
}
@end

@implementation ViideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrdata =[[NSMutableArray alloc] init];
    [self getDatawithoutchild:@"video"];
    [self.collectionvw registerNib:[UINib nibWithNibName:@"VideoCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"recipe"];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark- collection view data source
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    VideoCollectionViewCell *gridCell=[collectionView dequeueReusableCellWithReuseIdentifier:@"recipe" forIndexPath:indexPath];
    if ([self.strtype isEqualToString:@"Photos"]) {
        NSString * strurl = [NSString stringWithFormat:@"https://society.osoeasy.in/%@/%@",[[arrdata objectAtIndex:indexPath.row] valueForKey:@"FolderName"],[[arrdata objectAtIndex:indexPath.row] valueForKey:@"PhotoName"] ];
        gridCell.imgBack.image = nil;
        //gridCell.imgBack.imageURL = nil;
        dispatch_async(dispatch_get_global_queue(0,0), ^{
            NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:strurl]];
            if ( data == nil )
                return;
            dispatch_async(dispatch_get_main_queue(), ^{
                // WARNING: is the cell still using the same data by this point??
                gridCell.imgBack.image = [UIImage imageWithData: data];
            });
        });
    //gridCell.imgBack.imageURL =[NSURL URLWithString:strurl];
     
        gridCell.btnPlay.hidden = true;
        gridCell.imgPlay.hidden = true;
    }
    gridCell.imgBack.layer.borderColor =[UIColor whiteColor].CGColor;
    gridCell.imgBack.layer.borderWidth = 2;
    return gridCell;
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return arrdata.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    float temp ;
    temp = self.view.bounds.size.width;
    return CGSizeMake((collectionView.bounds.size.width-40)/3, (collectionView.bounds.size.width-40)/3);
    
}
#pragma mark - UICollectionViewDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (![self.strtype isEqualToString:@"Photos"]) {

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat: @"%@",[[arrdata objectAtIndex:indexPath.row] valueForKey:@"vd_vdlink"]]]];
    }
}
-(void)getDatawithoutchild :(NSString*)taskcode{
    
    
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:Token forKey:@"token"];
    [dict setObject:[self getuseremail] forKey:@"email"];
    HttpObject *http;
    if ([self.strtype isEqualToString:@"Photos"]) {
        http = [[HttpObject alloc] initWithUrl:[NSString stringWithFormat:@"%@PhotoGalleryList",BASE_URL] withTaskCode:taskcode];
    }
    else
    {
    http = [[HttpObject alloc] initWithUrl:[NSString stringWithFormat:@"%@VideoList",BASE_URL] withTaskCode:taskcode];
    }
 
    
    http.dicParams = dict;
    http.strMethodType = @"get";
    [self executeTask:http];
    
    
}


-(BOOL)onSuccess:(id)object forTaskCode:(NSString *)taskcode httpObject:(HttpObject *)objHttp{
    [super onSuccess:object forTaskCode:taskcode httpObject:objHttp];
    NSString *str=[NSString stringWithFormat:@"%@",[object valueForKey:@"message"]];
    if ([str isEqualToString:@"success"]) {
        
        
            arrdata = [object valueForKeyPath:@"data.table1"];
            
        
    }
   _collectionvw .reloadData;
    return YES;
}

-(void)onFailure:(HttpObject *)paramObject forTaskCode:(NSString *)taskCode{
    [super onFailure:paramObject forTaskCode:taskCode];
    NSLog(@"RT %@", taskCode);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

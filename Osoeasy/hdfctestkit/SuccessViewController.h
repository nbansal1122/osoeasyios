//
//  SuccessViewController.h
//  Demo
//
//  Created by Martin Prabhu on 5/25/16.
//  Copyright © 2016 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JASidePanelController.h"
#import "UIViewController+JASidePanel.h"
@class JASidePanelController;
@interface SuccessViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSDictionary *dict;
    
}
@property (strong, nonatomic) JASidePanelController *sidePanelObj;

@property(nonatomic,retain)NSMutableDictionary *jsondict;

@property(nonatomic,retain)IBOutlet UIScrollView *scroll;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UILabel *lblThanks;

@end

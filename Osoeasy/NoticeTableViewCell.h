//
//  NoticeTableViewCell.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/14/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoticeTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *vwtop;
@property (strong, nonatomic) IBOutlet UIImageView *iimg;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblDesc;
@property (strong, nonatomic) IBOutlet UILabel *lblTnoucment;

@end

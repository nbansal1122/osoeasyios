//
//  EventCellTableViewCell.m
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/13/16.
//  Copyright © 2016 Rahul Mehndiratta. All rights reserved.
//

#import "EventCellTableViewCell.h"

@implementation EventCellTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

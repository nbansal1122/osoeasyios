//
//  TicketTableViewCell.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/29/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbltitle;

@end

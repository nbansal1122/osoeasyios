//
//  AlarmVC.m
//  Osoeasy
//
//  Created by Apple on 14/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "AlarmVC.h"

@interface AlarmVC ()

@end

@implementation AlarmVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"Emergency Alarm";
    UIBarButtonItem *btnback= [[UIBarButtonItem alloc]
                               initWithImage:[UIImage imageNamed:@"arrow"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonBack)];
    self.navigationItem.leftBarButtonItem = btnback;

    
    
    UIBarButtonItem *btnEdit= [[UIBarButtonItem alloc]
                               initWithImage:[UIImage imageNamed:@"edit_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonAddAlarm)];
    self.navigationItem.rightBarButtonItem = btnEdit;

    // Do any additional setup after loading the view.
}


-(void)buttonAddAlarm{
    [self performSegueWithIdentifier:@"addMember" sender:nil];
}

-(void)buttonBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  LoginViewController.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/22/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JASidePanelController.h"
#import "UIViewController+JASidePanel.h"
#import "ViewController.h"
#import "MenuVC.h"
#import "HomeVC.h"
#import "CalanderVC.h"
#import "BaseVC.h"
@interface LoginViewController : BaseVC
@property (strong, nonatomic) IBOutlet UITextField *txtPass;
@property (strong, nonatomic) IBOutlet UITextField *txtemail;
@property (strong, nonatomic) JASidePanelController *sidePanelObj;
@property (strong, nonatomic) IBOutlet UIImageView *imgbg;

- (IBAction)signInAction:(id)sender;
@end

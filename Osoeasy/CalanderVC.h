//
//  CalanderVC.h
//  Osoeasy
//
//  Created by Apple on 13/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import  "FSCalendar.h"
#import "NSDate+FSExtension.h"
#import "AppDelegate.h"
#import "BaseVC.h"
@interface CalanderVC : BaseVC <FSCalendarDataSource, FSCalendarDelegate,FSCalendarDelegateAppearance>
@property (weak, nonatomic) IBOutlet FSCalendar *viewCalander;

@end

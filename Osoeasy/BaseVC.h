//
//  BaseVC.h
//  Chyc
//
//  Created by Nitin Bansal on 04/03/16.
//  Copyright (c) 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpinKit/RTSpinKitView.h>
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "AppConstants.h"
#import "DownloadManager.h"

@interface BaseVC : UIViewController<RestDelegate>
{
    DownloadManager *obj;
}
-(void)executeTask:(HttpObject *)http;
-(void)showAlert:(NSString *)msg;
-(void)startHudAnimating;
-(void)startHudAnimating:(NSString *)msg;
-(void)stopHudAnimating;
-(BOOL) NSStringIsValidEmail:(NSString *)checkString;
-(double)getPercentage:(double)totalamount amountpercent:(double)amountPercent;
-(NSString*)getuseremail;

@end

//
//  calendarViewController.m
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 10/4/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "calendarViewController.h"
#import "calendarTableViewCell.h"
@interface calendarViewController ()

@end

@implementation calendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrdata.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    calendarTableViewCell*	cell=[tableView dequeueReusableCellWithIdentifier:@"newsCell"];
    
    if (cell==nil) {
        cell=[[[NSBundle mainBundle] loadNibNamed:@"calendarTableViewCell" owner:self options:nil] objectAtIndex:0];
    }
    cell.lblTitle.text =[NSString stringWithFormat:@"%@",[[_arrdata objectAtIndex:indexPath.row] valueForKey:@"ca_title"]];
      cell.lblDesc.text =[NSString stringWithFormat:@"%@",[[_arrdata objectAtIndex:indexPath.row] valueForKey:@"ca_details"]];
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return  55;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    
    return 1;//Or return whatever as you intend
}

@end

//
//  CalenderCollectionViewCell.h
//  Guiden
//
//  Created by Rahul Mehndiratta on 7/25/16.
//  Copyright © 2016 RahulMehndiratta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalenderCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *labelInstructions;
@property (weak, nonatomic) IBOutlet UILabel *labelViewDetails;
@property (strong, nonatomic) IBOutlet UILabel *lbltitle;

@end

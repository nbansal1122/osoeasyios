//
//  EventCellTableViewCell.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/13/16.
//  Copyright © 2016 Rahul Mehndiratta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewTop;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblPublish;
@property (strong, nonatomic) IBOutlet UILabel *lblEnddate;
@property (strong, nonatomic) IBOutlet UIButton *btnClose;
@property (strong, nonatomic) IBOutlet UILabel *lblrate;

@end

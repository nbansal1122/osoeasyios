//
//  HeaderTableViewCell.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/14/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface HeaderTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblemail;
@property (strong, nonatomic) IBOutlet UILabel *lblphone;

@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet AsyncImageView *imgUser;
@end

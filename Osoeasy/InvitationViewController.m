//
//  InvitationViewController.m
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/14/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "InvitationViewController.h"
#import "EasyPollViewController.h"
@interface InvitationViewController ()
{
    UIDatePicker* picker;
}
@end

@implementation InvitationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    picker = [[UIDatePicker alloc] init];
    picker.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    picker.datePickerMode = UIDatePickerModeDate;
    
    [picker addTarget:self action:@selector(dueDateChanged:) forControlEvents:UIControlEventValueChanged];
    CGSize pickerSize = [picker sizeThatFits:CGSizeZero];
    picker.frame = CGRectMake(0.0, self.view.frame.size.height - 200, self.view.frame.size.width, 200);
    picker.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:picker];
    UIBarButtonItem *btnback= [[UIBarButtonItem alloc]
                               initWithImage:[UIImage imageNamed:@"arrow"] style:UIBarButtonItemStylePlain target:self action:@selector(buttonBackAddMember)];
    self.navigationItem.leftBarButtonItem = btnback;
    
//    _txtDate.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    _txtDate.layer.borderWidth = 1;
//    
//    _txtdescc.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    _txtdescc.layer.borderWidth = 1;
//    
//    _txtTitle.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    _txtTitle.layer.borderWidth = 1;
//    _txtTitle.layer.cornerRadius = 10;
//    _txtdescc.layer.cornerRadius = 10;
//    _txtDate.layer.cornerRadius = 10;

   
    // Do any additional setup after loading the view from its nib.
}
-(void)viewDidAppear:(BOOL)animated
{
    _txtDate.text =@"";
    _txtdescc.text =@"";
    _txtTitle.text=@"";
    
   }
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)buttonBackAddMember{
    [self.navigationController popViewControllerAnimated:YES];
    //  [self performSegueWithIdentifier:@"addMember" sender:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnNextACtion:(id)sender {
    [self showDatePicker:nil];
}
-(void)showDatePicker:(id)sender
{
    
    picker.hidden = false;
   
    
    
}

-(void) dueDateChanged:(UIDatePicker *)sender {
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    picker.hidden = true;
    self.txtDate.text = [dateFormatter stringFromDate:[picker date]];
    NSLog(@"Picked the date %@", [dateFormatter stringFromDate:[sender date]]);
}
- (IBAction)nextscreenAction:(id)sender {
    
    if (_txtTitle.text.length == 0) {
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter Title" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else  if (_txtdescc.text.length == 0) {
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter Description" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else  if (_txtDate.text.length == 0) {
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter Date" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        EasyPollViewController *objEasyPollViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"easyPollView"];
        objEasyPollViewController.strType = @"invite";
        objEasyPollViewController.strInviteDate = _txtDate.text;
        objEasyPollViewController.strinviteDesc = _txtdescc.text;
        objEasyPollViewController.strInviteTitle = _txtTitle.text;

        [self.navigationController pushViewController:objEasyPollViewController animated:nil];
        picker.hidden = true;
        
    }
    

}
@end

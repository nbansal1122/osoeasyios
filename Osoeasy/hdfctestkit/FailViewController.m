//
//  FailViewController.m
//  Demo
//
//  Created by Martin Prabhu on 5/25/16.
//  Copyright © 2016 test. All rights reserved.
//

#import "FailViewController.h"
#import "SocityBillViewController.h"
#import "SucessViewTableViewCell.h"
#import "AppDelegate.h"
@interface FailViewController ()
{
    NSArray*arraydata;
}
@end

@implementation FailViewController
@synthesize jsondict;
@synthesize session;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Config" ofType:@"plist"];
    dictPlist = [[NSDictionary alloc] initWithContentsOfFile:path];
    
    // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ResponseNew:) name:@"FAILED_DICT" object:nil];
    UIBarButtonItem *btnMenuAction = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow"]style: UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    //    [btnMenuAction setTintColor:kCIAttributeDefault];
    
    self.navigationItem.leftBarButtonItem = btnMenuAction;
    arraydata =[NSArray arrayWithObjects:@"Billinng Name:",@"Transaction Id:",@"Description:",@"Amount:",@"Detail of Apartment:",@"Merchant Reference:",@"Payment Id:",@"Mobile Number:",@"Email:",@"ResponseMessage:", nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ResponseNew:) name:@"JSON_NEW" object:nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"JSON_DICT" object:nil userInfo:nil];

}
-(void)backAction
{
    //[self dismissViewControllerAnimated:YES completion:nil];
    
    SocityBillViewController *obj=[self.storyboard instantiateViewControllerWithIdentifier:@"societyView"];
    self.sidePanelController.centerPanel = [[UINavigationController alloc]initWithRootViewController:obj];
    //    SocityBillViewController *obj=[self.storyboard instantiateViewControllerWithIdentifier:@"societyView"];
    //    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    self.sidePanelObj = [[JASidePanelController alloc]init];
    //    self.sidePanelObj.shouldDelegateAutorotateToVisiblePanel = NO;
    //    self.sidePanelObj.leftPanel = [storyboard instantiateViewControllerWithIdentifier:@"menuView"];
    //    self.sidePanelObj.centerPanel = [[UINavigationController alloc] initWithRootViewController:obj];
    //    [[[UIApplication sharedApplication].delegate window] setRootViewController:self.sidePanelObj];
    
    
}
-(void) ResponseNew:(NSNotification *)message
{
    if ([message.name isEqualToString:@"JSON_NEW"])
    {
        NSLog(@"Response json data = %@",[message object]);
        
        jsondict = [message object];
    }
    
  //  [self createView];
    
}
-(IBAction)tryAgainAction:(id)sender
{
    [self callDictionary];
    
    PaymentModeViewController *paymentView=[[PaymentModeViewController alloc]init];
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    paymentView.paymentAmtString =[defaults objectForKey:@"paymentAmtString"];
    paymentView.descriptionString = [defaults objectForKey:@"descriptionString"];
    
    paymentView.strSaleAmount =[defaults objectForKey:@"strSaleAmount"];
    
    paymentView.strCurrency =[defaults objectForKey:@"strCurrency"];
    
    paymentView.strDisplayCurrency =[defaults objectForKey:@"strDisplayCurrency"];
    
    paymentView.strDescription =[defaults objectForKey:@"strDescription"];
    
    paymentView.strBillingName = [defaults objectForKey:@"strBillingName"];
    
    paymentView.strBillingAddress = [defaults objectForKey:@"strBillingAddress"];
    
    paymentView.strBillingCity =[defaults objectForKey:@"strBillingCity"];
    
    paymentView.strBillingState = [defaults objectForKey:@"strBillingState"];
    paymentView.strBillingPostal =[defaults objectForKey:@"strBillingPostal"];
    
    paymentView.strBillingCountry = [defaults objectForKey:@"strBillingCountry"];
    
    paymentView.strBillingEmail =[defaults objectForKey:@"strBillingEmail"];
    
    paymentView.strBillingTelephone =[defaults objectForKey:@"strBillingTelephone"];
    
    paymentView.strDeliveryName =[defaults objectForKey:@"strDeliveryName"];
    
    paymentView.strDeliveryAddress = [defaults objectForKey:@"strDeliveryAddress"];
    
    paymentView.strDeliveryCity =[defaults objectForKey:@"strDeliveryCity"];
    paymentView.strDeliveryState =[defaults objectForKey:@"strDeliveryState"];
    
    paymentView.strDeliveryPostal =[defaults objectForKey:@"strDeliveryPostal"];
    
    paymentView.strDeliveryCountry =[defaults objectForKey:@"strDeliveryCountry"];
    
    paymentView.strDeliveryTelephone =[defaults objectForKey:@"strDeliveryTelephone"];
    
    paymentView.reference_no=[defaults objectForKey:@"reference_no"];
    
    paymentView.session = [defaults objectForKey:@"session"];
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    for (UIViewController *aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:[PaymentModeViewController class]]) {
            [self.navigationController popToViewController:aViewController animated:NO];
        }
    }
}

-(void)callDictionary
{
    MRMSDevFPiOS *mrms = [[MRMSDevFPiOS alloc] initWithDemo:NO];
    
    session = [mrms createSession];
    
    NSDictionary *parameters = @{@"sid" : session,@"aid" :@"10375"};
    
    NSDictionary *result = [mrms callDeviceAPIwithParameters:parameters];
    
    // NSLog(@"DeviceAPI Result in paymentView controller:%@",result);
    
    
}

- (void)createView
{
    UIScrollView *scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height-140)];
    scrollview.backgroundColor=[UIColor clearColor];
    
    float redValues  =  [[[dictPlist valueForKey:@"BUTTON_BG_COLOR"] valueForKey:@"Red"] floatValue];
    float greenValues = [[[dictPlist valueForKey:@"BUTTON_BG_COLOR"] valueForKey:@"Green"]floatValue];
    float blueValues = [[[dictPlist valueForKey:@"BUTTON_BG_COLOR"] valueForKey:@"Blue"]floatValue];
    float alphaValues =[[[dictPlist valueForKey:@"BUTTON_BG_COLOR"] valueForKey:@"alpha"]floatValue];
    
    NSArray *keyArray=[jsondict allKeys];
    
    int x,gap,height,ypos = 0;
    int font_size;
    int labelWIdth;
    int tryBtnXPOS;
    int tryBtnWidth;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        // iPad
        x=20,gap=20,height=40;
        font_size=17;
        labelWIdth=160;
        font_size=21;
        
        tryBtnXPOS=150;
        tryBtnWidth=180;
        
    }
    else
    {
        // iPhone
        x=10,gap=10,height=30;
        font_size=13;
        labelWIdth=120;
        font_size=14;
        tryBtnXPOS=20;
        tryBtnWidth=120;
        
    }
    
    
    for (int i=0;i<[keyArray count];i++)
    {
        NSString * responseString =[jsondict objectForKey:[keyArray objectAtIndex:i]];
        
        UILabel *listLabel = [[UILabel alloc]initWithFrame:CGRectMake(x, ypos, self.view.frame.size.width-x*2, height)];
        
        listLabel.font = [UIFont fontWithName:@"Helvetica" size:font_size];
        
        listLabel.text=[NSString stringWithFormat:@"%@ : %@",[keyArray objectAtIndex:i],responseString];
        
        listLabel.backgroundColor = [UIColor clearColor];
        listLabel.textColor = [UIColor blackColor];
        
        listLabel.textAlignment = NSTextAlignmentLeft;
        
        ypos=listLabel.frame.origin.y+listLabel.frame.size.height+gap;
        
        [scrollview addSubview:listLabel];
    }
    
    int btnXPOS = self.view.frame.size.width/2-60;
    // ypos=self.view.frame.size.height-70;
    
    UIButton *TryAgain = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    TryAgain.frame = CGRectMake(tryBtnXPOS, ypos, tryBtnWidth, 40);
    [TryAgain addTarget:self action:@selector(tryAgainAction:) forControlEvents:UIControlEventTouchUpInside];
    //TryAgain.backgroundColor = [UIColor colorWithRed:0/255.0 green:120/255.0 blue:191/255.0 alpha:1.0];
    TryAgain.backgroundColor= [UIColor colorWithRed:redValues/255.0 green:greenValues/255.0 blue:blueValues/255.0 alpha:alphaValues];
    [scrollview addSubview:TryAgain];
    
    // Submit Button Label
    UILabel *TryAgainbtnLabel= [[UILabel alloc]initWithFrame:CGRectMake(0, 0, TryAgain.frame.size.width, TryAgain.frame.size.height)];
    TryAgainbtnLabel.text=@"Try Again";
    TryAgainbtnLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:font_size+3];
    TryAgainbtnLabel.textColor=[UIColor whiteColor];
    TryAgainbtnLabel.textAlignment = NSTextAlignmentCenter;
    TryAgainbtnLabel.backgroundColor=[UIColor clearColor];
    [TryAgain addSubview:TryAgainbtnLabel];
    
    scrollview.contentSize = CGSizeMake(self.view.frame.size.width,ypos+100);
    
    [self.view addSubview:scrollview];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    cancelBtn.frame = CGRectMake(self.view.frame.size.width-tryBtnWidth-tryBtnXPOS-gap, ypos, tryBtnWidth, 40);
    [cancelBtn addTarget:self action:@selector(cancelClk:) forControlEvents:UIControlEventTouchUpInside];
    //cancelBtn.backgroundColor = [UIColor colorWithRed:0/255.0 green:120/255.0 blue:191/255.0 alpha:1.0];
    cancelBtn.backgroundColor= [UIColor colorWithRed:redValues/255.0 green:greenValues/255.0 blue:blueValues/255.0 alpha:alphaValues];
    [scrollview addSubview:cancelBtn];
    
    // Submit Button Label
    UILabel *cancelBtnLabel= [[UILabel alloc]initWithFrame:CGRectMake(0, 0, cancelBtn.frame.size.width, cancelBtn.frame.size.height)];
    cancelBtnLabel.text=@"Cancel";
    cancelBtnLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:font_size+3];
    cancelBtnLabel.textColor=[UIColor whiteColor];
    cancelBtnLabel.textAlignment = NSTextAlignmentCenter;
    cancelBtnLabel.backgroundColor=[UIColor clearColor];
    [cancelBtn addSubview:cancelBtnLabel];
    
    scrollview.contentSize = CGSizeMake(self.view.frame.size.width,ypos+100);
    
    [self.view addSubview:scrollview];
    
}


- (IBAction)cancelClk:(id)sender
{
    UIResponder *responder = self;
    
    while (![responder isKindOfClass:[UIViewController class]])
    {
        responder = [responder nextResponder];
        if (nil == responder)
        {
            break;
        }
    }
    UIViewController *viewController = (UIViewController *) responder;
    
    UIViewController *viewCont =     [[NSClassFromString([dictPlist objectForKey:@"CANCEL_VIEWCONTROLLER"]) alloc] init];
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:[[NSBundle mainBundle].infoDictionary objectForKey:@"UIMainStoryboardFile"] bundle:[NSBundle mainBundle]];
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    @try {
        
        if (storyBoard != nil)
        {
            storyBoard =  viewController.storyboard;
            
            UIStoryboard *storyBoard = viewController.storyboard;
            
            viewCont = [storyBoard instantiateViewControllerWithIdentifier:[dictPlist objectForKey:@"CANCEL_VIEWCONTROLLER"]];
            
            int index = 0;
            bool status=false;
            
            for (int i=0;i<[allViewControllers count];i++)
            {
                NSString *strClass = NSStringFromClass([[allViewControllers objectAtIndex:i] class]);
                if([strClass isEqualToString:CANCEL_VIEWCONTROLLER])
                {
                    index=i;
                    status=true;
                }
            }
            if (status)
            {
                [self.navigationController popToViewController:(UIViewController *)[allViewControllers objectAtIndex:index] animated:NO];
            }
            else{
                [self.navigationController pushViewController:viewCont animated:NO];
            }
            
        }
        else
        {
            int index = 0;
            bool status=false;
            
            for (int i=0;i<[allViewControllers count];i++)
            {
                NSString *strClass = NSStringFromClass([[allViewControllers objectAtIndex:i] class]);
                if([strClass isEqualToString:CANCEL_VIEWCONTROLLER])
                {
                    index=i;
                    status=true;
                }
            }
            if (status)
            {
                [self.navigationController popToViewController:(UIViewController *)[allViewControllers objectAtIndex:index] animated:NO];
            }
            else{
                [self.navigationController pushViewController:viewCont animated:NO];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"You are entered invalid CANCEL_VIEWCONTROLLER");
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arraydata.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    SucessViewTableViewCell*	cell=[tableView dequeueReusableCellWithIdentifier:@"newsCell"];
    
    if (cell==nil) {
        cell=[[[NSBundle mainBundle] loadNibNamed:@"SucessViewTableViewCell" owner:self options:nil] objectAtIndex:0];
    }
    
    cell.frame = CGRectMake(0, 0, self.view.bounds.size.width, 40);
    cell.lblName.text =[arraydata objectAtIndex:indexPath.row];
    switch (indexPath.row) {
        case 0:
            
            cell.lblValue.text = [NSString stringWithFormat:@"%@",[jsondict objectForKey:@"BillingName"]];
            
            
            
            break;
        case 1:
            
            cell.lblValue.text = [NSString stringWithFormat:@"%@",[jsondict objectForKey:@"TransactionId"]];
            
            
            
            break;
            
        case 2:
            cell.lblValue.text = [NSString stringWithFormat:@"OSOeasy Socity Bill Payment"];
            
            
            break;
            
        case 3:
            cell.lblValue.text = [NSString stringWithFormat:@"%@",[jsondict objectForKey:@"Amount"]];
            
            
            break;
            
        case 4:
            cell.lblValue.text = [NSString stringWithFormat:@"%@",[jsondict objectForKey:@"BillingAddress"]];
            cell.lblValue.backgroundColor = [UIColor orangeColor];
            cell.frame = CGRectMake(0, 0, self.view.bounds.size.width, 70);

            cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x, 0, cell.lblValue.frame.size.width, 70);
            break;
            
            
        case 5:
            cell.lblValue.text = [NSString stringWithFormat:@"%@",[jsondict objectForKey:@"MerchantRefNo"]];
            break;
            
            
        case 6:
            cell.lblValue.text = [NSString stringWithFormat:@"%@",[jsondict objectForKey:@"PaymentId"]];
            
            break;
            
            
        case 7:
            cell.lblValue.text = [NSString stringWithFormat:@"%@",[jsondict objectForKey:@"BillingPhone"]];
            break;
            
        case 8:
            cell.lblValue.text = [NSString stringWithFormat:@"%@",[jsondict objectForKey:@"BillingEmail"]];
            
            
            break;
        case 9:
            cell.lblValue.text = [NSString stringWithFormat:@"%@",[jsondict objectForKey:@"ResponseMessage"]];
            
            
            break;
        default:
            break;
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
    
    
}

-(void)m_ShadowWithView:(UIView *)view{
    view.layer.masksToBounds = NO;
    // view.layer.cornerRadius = 8; // if you like rounded corners
    view.layer.shadowOffset = CGSizeMake(0,1);
    view.layer.shadowRadius = 1.0;
    view.layer.shadowOpacity = 0.5;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (indexPath.row == 4) {
        return 70;
    }
    return 40;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

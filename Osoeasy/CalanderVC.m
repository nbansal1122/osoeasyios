//
//  CalanderVC.m
//  Osoeasy
//
//  Created by Apple on 13/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "CalanderVC.h"
#import "calendarViewController.h"
@interface CalanderVC ()
{
    NSMutableArray *arrdata,*arrdate;
}
@end

@implementation CalanderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    arrdata = [[NSMutableArray alloc] init];
    arrdate = [[NSMutableArray alloc] init];

    [self getData];
    // UIView *view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
  //  FSCalendar *calendar = [[FSCalendar alloc] initWithFrame:CGRectMake(0, 64, view.frame.size.width, view.frame.size.height-280)];
    _viewCalander.dataSource = self;
    _viewCalander.delegate = self;
    _viewCalander.backgroundColor=[UIColor whiteColor];
    _viewCalander.selectedDate = [NSDate date];
    _viewCalander.appearance.titlePlaceholderColor=[UIColor whiteColor];
    _viewCalander.appearance.titleDefaultColor=[UIColor orangeColor];
    _viewCalander.appearance.headerTitleColor=[UIColor lightGrayColor];
    _viewCalander.appearance.weekdayFont=[UIFont systemFontOfSize:12.0];
    _viewCalander.appearance.titleFont=[UIFont systemFontOfSize:8.0];
    _viewCalander.appearance.weekdayTextColor =[UIColor lightGrayColor];
  //  [self.view addSubview:_viewCalander];
    
  // [AppDelegate creatingCustomButton:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - FSCalendarDelegate

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date
{
    NSString*strtemp = [date fs_stringWithFormat:@"yyyy-MM-dd"];
  NSLog(@"did select date %@",[date fs_stringWithFormat:@"yyyy-MM-dd"]);
    
    NSMutableArray *arrtemp =[[NSMutableArray alloc] init] ;
    for (int i=0; i<arrdata.count; i++) {
        if ([[arrdate objectAtIndex:i] isEqualToString:strtemp]) {
            [arrtemp addObject:[arrdata objectAtIndex:i]];
        }
    }
    calendarViewController *objcal =[[calendarViewController alloc] initWithNibName:@"calendarViewController" bundle:nil];
    objcal.arrdata = arrtemp;
    [self.navigationController pushViewController:objcal animated:NO];
    
}

- (void)calendarCurrentMonthDidChange:(FSCalendar *)calendar
{
     NSLog(@"did change to month %@",[calendar.currentMonth fs_stringWithFormat:@"MMMM yyyy"]);
}



-(void)doAction:(id)sender{
    AlarmVC *objHomeVC=[self.storyboard instantiateViewControllerWithIdentifier:@"alarmView"];
    [self.navigationController pushViewController:objHomeVC animated:YES];
}
- (UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance fillDefaultColorForDate:(NSDate *)date
{
    
    NSString *key = [calendar stringFromDate:date format:@"yyyy-MM-dd"];
   
    if ([arrdate containsObject:key]) {
        return  [[UIColor lightGrayColor] colorWithAlphaComponent:0.7f];
    }
    return nil;
}
-(BOOL)onSuccess:(id)object forTaskCode:(NSString *)taskcode httpObject:(HttpObject *)objHttp{
    [super onSuccess:object forTaskCode:taskcode httpObject:objHttp];
    NSString *str=[NSString stringWithFormat:@"%@",[object valueForKey:@"message"]];
    if (![str isEqualToString:@"success"]) {
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:[object valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
                arrdata = [object valueForKeyPath:@"data.table1"];
        [self convertdateFormat ];
        
           }
    
    return YES;
}

-(void)onFailure:(HttpObject *)paramObject forTaskCode:(NSString *)taskCode{
    [super onFailure:paramObject forTaskCode:taskCode];
    NSLog(@"RT %@", taskCode);
}


-(void)getData{
      
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:Token forKey:@"token"];
    [dict setObject:[self getuseremail] forKey:@"email"];
    
    [dict setObject:@"" forKey:@"cond"];
    
    [dict setObject:@"caLcalendar" forKey:@"tblname"];
    HttpObject *http = [[HttpObject alloc] initWithUrl:[NSString stringWithFormat:@"%@operation",BASE_URL] withTaskCode:@"calendar"];
    http.dicParams = dict;
    http.strMethodType = @"get";
    [self executeTask:http];
    
   
}
-(void)convertdateFormat
{
    for (int i = 0; i<arrdata.count; i++) {
        NSString *stringDate =[NSString stringWithFormat:@"%@",[[arrdata objectAtIndex:i] valueForKey:@"ca_sdate"]];
        NSArray *arrtemp =[stringDate componentsSeparatedByString:@"T"];
        [arrdate addObject:[arrtemp objectAtIndex:0]];
    }
    [_viewCalander reloadData];
}
@end

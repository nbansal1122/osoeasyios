//
//  customCell.h
//  Osoeasy
//
//  Created by Apple on 17/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblMobileValue;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblMobileNo;
@property (strong, nonatomic) IBOutlet UILabel *lblFlat;
@property (strong, nonatomic) IBOutlet UILabel *lblEmailValue;
@property (strong, nonatomic) IBOutlet UILabel *lblFlatValue;
@property (strong, nonatomic) IBOutlet UILabel *lblresidenceValue;
@property (strong, nonatomic) IBOutlet UILabel *lblMemberValue;
@property (strong, nonatomic) IBOutlet UILabel *lblrelationshipValue;
@property (strong, nonatomic) IBOutlet UILabel *lblrelationship;
@property (strong, nonatomic) IBOutlet UILabel *lblFamily;
@property (strong, nonatomic) IBOutlet UILabel *lblResidence;
@property (strong, nonatomic) IBOutlet UIView *vwintrnl;

@end

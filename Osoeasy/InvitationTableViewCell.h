//
//  InvitationTableViewCell.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/14/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InvitationTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *vwTop;
@property (strong, nonatomic) IBOutlet UIView *vwtt;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblname;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;

@end

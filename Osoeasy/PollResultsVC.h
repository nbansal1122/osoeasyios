//
//  PollResultsVC.h
//  Osoeasy
//
//  Created by Apple on 15/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
@interface PollResultsVC : BaseVC
- (IBAction)addopinionAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UITableView *tblopinion;
- (IBAction)submitaction:(id)sender;
- (IBAction)hideopinionview:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *vwopinion;
@property (strong, nonatomic)  NSString *strpoolid;

@end

//
//  AppConstants.h
//  ChycApp
//
//  Created by Nitin Bansal on 21/02/16.
//  Copyright (c) 2016 Nitin Bansal. All rights reserved.
//

#ifndef ChycApp_AppConstants_h
#define ChycApp_AppConstants_h



#endif
@interface AppConstants : NSObject


#define BASE_URL @"http://osoapi.azurewebsites.net/api/"



// Page URls
#define PHOTO_PROFILE @"PhotoProfile"
#define USER_PROFILE @"UserProfile"
#define Token @"abc456"
#define NOTICE_BOARD @"NoticeBoard"
#define COMPLAIN_STATUS @"ComplaintsStatusList"
#define PARKING_ALLOTMENT @"ParkingAllotment"

#define DASH_BOARD_URL @"Dashboard"

#define EASY_POLL_1 @"OpinionPoll"
#define LOGIN_URL @"Login"
#define InvitationList @"InvitationList"
#define USER_DATA @"users/"
#define DEVICE_REGISTER @"messaging/registrations"
#define Default_configuration @"data/Config"
#define FEEDBACK @"data/Feedback"
#define SUBSCRIPTION @"data/Subscription"


//User Defaults Keys
#define USER_PROFILE @"userProfile"
#define SERVICE_VISITED_ARRAY @"servicesVisitedArray"


// Color Constants
#define RGB(r,g,b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]
#define CLR_cyan RGB(0,205,204)
#define CLR_TAB_TEXT RGB(163,163,163)
#define CLR_TAB_BG RGB(23,23,23)
#define CLR_WHITE RGB(255,255,255)

// UI Constants
#define MOBILE_NOT_AVAILBLE @"Mobile N/A"
#define ADDRESS_NOT_AVAILBLE @"Enter your address"
#define HINT_ENTER_MOBILE @"Mobile Number"
#define HINT_ADDRESS @"Address"
@end
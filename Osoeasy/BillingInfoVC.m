//
//  BillingInfoVC.m
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 11/2/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "BillingInfoVC.h"
#import "WebViewController1.h"
#import "PaymentModeViewController.h"
@interface BillingInfoVC ()

@end

@implementation BillingInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setborder:_txtzipcode];
    [self setborder:_txtAddress];
    [self setborder:_txtState];
    [self setborder:_txtEmail];
    [self setborder:_txtCity];
    [self setborder:_txtName];
    [self setborder:_txtMobileNumber];
    [self setborder:_txtSocity];

    _txtCity.text = _billing_city;
    _txtName.text = _billing_name;
    _txtEmail.text = _billing_email;
    _txtState.text = _billing_state;
    _txtAddress.text = _billing_address;
    _txtMobileNumber.text = _billing_tel;
    _scrlvw.contentSize = CGSizeMake(0, 550);
    if (![_strpage isEqualToString:@"ccvenue"]) {
        [_btnpayment setTitle:@"Socity Bill Payment" forState:UIControlStateNormal];
    }
    [[self navigationController] setNavigationBarHidden:YES animated:YES];

    // Do any additional setup after loading the view.
}
-(void)setborder:(UITextField*)fld
{
    fld.layer.borderWidth = 1;
    fld.layer.borderColor = [UIColor lightGrayColor].CGColor;
    fld.layer.cornerRadius = 5;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    [textField  resignFirstResponder];
    return YES;
}
- (IBAction)ProccedAction:(id)sender {
    if (_txtMobileNumber.text.length == 0 || _txtName.text.length == 0 ||_txtCity.text.length == 0|| _txtEmail.text.length == 0 ||_txtState.text.length == 0|| _txtAddress.text.length == 0 ||_txtzipcode.text.length == 0||_txtSocity.text.length == 0) {
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please fill all values." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
else
{
    if ([_strpage isEqualToString:@"ccvenue"]) {
        
    
       
    WebViewController1* controller = [[WebViewController1 alloc] initWithNibName:@"WebViewController1" bundle:nil];
    int r = rand();
    controller.accessCode = @"AVCS64DD72AH63SCHA";
    controller.merchantId = @"94038";
    controller.amount = _amount;
         //   controller.amount = @"1";
    
    controller.currency = @"INR";
    controller.orderId = [NSString stringWithFormat:@"%d",r];
    controller.redirectUrl = @"http://osoapi.azurewebsites.net/api/CCAvenuerRSASuccess/CCSucess";
    controller.cancelUrl = @"http://osoapi.azurewebsites.net/api/CCAvenuerRSACancel";
    controller.rsaKeyUrl = @"http://osoapi.azurewebsites.net/api/CCAvenuerRSA";
    
    
    controller.billing_tel = _txtMobileNumber.text;
    controller.billing_zip = _txtzipcode.text;
    controller.billing_name = _txtName.text;
    controller.billing_city = _txtCity.text;
    controller.billing_email= _txtEmail.text;
    controller.billing_state = _txtState.text;
    controller.billing_address = _txtAddress.text;
    controller.billing_country = _txtzipcode.text;
        controller.billingSocityName = _txtSocity.text;
    //        controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self.navigationController pushViewController:controller animated:YES];

   // [self presentViewController:controller animated:YES completion:nil];
    }
    
    else
    {
    
        NSString *qtyString;
        float price ,priceString;
        
        priceString = 3.0;
        
        price =[_amount floatValue];
        
        //Merchant has to configure the below code
        
        PaymentModeViewController *paymentView=[[PaymentModeViewController alloc]init];
        // Mandatory Parameters
        // Price has to be configured
        NSString *strrandom =[NSString stringWithFormat:@"%d",arc4random_uniform(9999)];
        paymentView.paymentAmtString = [NSString stringWithFormat:@"%.2f",price];
        
        paymentView.strCurrency =@"INR";
        paymentView.strDisplayCurrency =@"USD";
        
        //Reference no has to be configured
        paymentView.reference_no = strrandom;
        
        paymentView.strDescription = @"Socity Bill Payment";
        
        paymentView.strBillingName = _txtName.text;
        paymentView.strBillingAddress = _txtAddress.text;
        paymentView.strBillingCity =_txtCity.text;
        paymentView.strBillingState = _txtState.text;
        paymentView.strBillingPostal =_txtzipcode.text;
        paymentView.strBillingCountry = @"IND";
        paymentView.strBillingEmail =_txtEmail.text;
        paymentView.strBillingTelephone =_txtMobileNumber.text;
        
        // Non mandatory parameters
        paymentView.strDeliveryName = _txtSocity.text;
        paymentView.strDeliveryAddress = @"";
        paymentView.strDeliveryCity = @"";
        paymentView.strDeliveryState = @"";
        paymentView.strDeliveryPostal =@"";
        paymentView.strDeliveryCountry = @"";
        paymentView.strDeliveryTelephone =@"";
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        [defaults setObject:strrandom forKey:@"reference_no"];
        [defaults setObject:[NSString stringWithFormat:@"%.2f",price] forKey:@"strSaleAmount"];
        paymentView.strSaleAmount =[NSString stringWithFormat:@"%.2f",price];
        
        [defaults synchronize];
        
        
        [self.navigationController pushViewController:paymentView animated:NO];
    }
}
}
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
   // [self dismissViewControllerAnimated:NO completion:nil];
}
@end

//
//  AddNewTicketViewController.m
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/29/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "AddNewTicketViewController.h"
#import "TicketTableViewCell.h"
@interface AddNewTicketViewController ()
{
    NSArray *arroption;
    NSString *selectcode;
}
@end

@implementation AddNewTicketViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self getData];
    selectcode = @"000001";
   // arroption =[[NSArray alloc] initWithObjects:@"ISSUE REGARDING GARDEN",@"ISSUE REGARDING GYM",@"ISSUE REGARDING ROAD",@"ISSUE REGARDING SWIMMING POOL",@"ISSUE REGARDING WATER",@"OTHER ISSUE", nil];
    [_tableView reloadData];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backACtion:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)actionIssue:(id)sender {
    self.vwOption.hidden = false;
}
- (IBAction)hideoptionView:(id)sender {
    self.vwOption.hidden = true;

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arroption.count;
}

- (float)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    // This will create a "invisible" footer
    return 0.01f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TicketTableViewCell*	cell=[tableView dequeueReusableCellWithIdentifier:@"newsCell"];
    
    if (cell==nil) {
        cell=[[[NSBundle mainBundle] loadNibNamed:@"TicketTableViewCell" owner:self options:nil] objectAtIndex:0];
    }

    
    cell.backgroundColor=[UIColor clearColor];
    cell.lbltitle.text =[[arroption objectAtIndex:indexPath.row] valueForKey:@"Description"];
    //cell.imageView.image=[UIImage imageNamed:[arrayImages objectAtIndex:indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.vwOption.hidden = true;
    [self.btnIssue setTitle:[[arroption objectAtIndex:indexPath.row] valueForKey:@"Description"] forState:UIControlStateNormal];
    selectcode = [[arroption objectAtIndex:indexPath.row] valueForKey:@"Code"];
}
-(BOOL)onSuccess:(id)object forTaskCode:(NSString *)taskcode httpObject:(HttpObject *)objHttp{
    [super onSuccess:object forTaskCode:taskcode httpObject:objHttp];
    NSString *str=[NSString stringWithFormat:@"%@",[object valueForKey:@"message"]];
    if (![str isEqualToString:@"success"]) {
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:[object valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        if ([taskcode isEqualToString:@"sendticket"]) {
            UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Ticket has been submitted" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            _txtbrif.text =@"";
            _txtDetail.text = @"";
        }
        arroption = [object valueForKeyPath:@"data.table1"];
        [_tableView reloadData];
        
    }
    
    return YES;
}

-(void)onFailure:(HttpObject *)paramObject forTaskCode:(NSString *)taskCode{
    [super onFailure:paramObject forTaskCode:taskCode];
    NSLog(@"RT %@", taskCode);
}


-(void)getData{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:Token forKey:@"token"];
    [dict setObject:[self getuseremail] forKey:@"email"];

    HttpObject *http = [[HttpObject alloc] initWithUrl:[NSString stringWithFormat:@"%@CompType",BASE_URL] withTaskCode:@"opinioncell"];
    http.dicParams = dict;
    http.strMethodType = @"get";
    [self executeTask:http];
    
    
}
-(void)sendTicketbutton{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:Token forKey:@"token"];
    [dict setObject:[self getuseremail] forKey:@"email"];
    [dict setObject:selectcode forKey:@"CompType"];
    [dict setObject:_txtbrif.text forKey:@"Subject"];
    [dict setObject:_txtDetail.text forKey:@"ComplaintsDetails"];

    HttpObject *http = [[HttpObject alloc] initWithUrl:[NSString stringWithFormat:@"%@ComplaintsTicketLog",BASE_URL] withTaskCode:@"sendticket"];
    http.dicParams = dict;
    http.strMethodType = @"get";
    [self executeTask:http];
    
    
}

- (IBAction)sendTicket:(id)sender {
    if (_txtDetail.text.length == 0) {
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter detail" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else  if (_txtbrif.text.length == 0) {
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter Subject" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [self sendTicketbutton];
    }
}
@end

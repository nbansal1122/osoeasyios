//
//  pollResultTableViewCell.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 10/2/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TYMProgressBarView.h"

@interface pollResultTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbltitle;
@property (strong, nonatomic) IBOutlet TYMProgressBarView *vwProgress;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio;

@end

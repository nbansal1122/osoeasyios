//
//  LoginViewController.m
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/22/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "LoginViewController.h"
@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _imgbg.clipsToBounds = true;
    _txtPass.text = @"amitbhadale";
    _txtemail.text = @"08585656545";
//    _txtemail.text =@"ACCOUNT_KUKREJA";
//    _txtPass.text = @"12345678";
    [[self navigationController] setNavigationBarHidden:YES animated:YES];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)signInAction:(id)sender {
//    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    HomeVC *objHomeVC=[storyboard instantiateViewControllerWithIdentifier:@"homeView"];
//    self.sidePanelObj = [[JASidePanelController alloc]init];
//    self.sidePanelObj.shouldDelegateAutorotateToVisiblePanel = NO;
//    self.sidePanelObj.leftPanel = [storyboard instantiateViewControllerWithIdentifier:@"menuView"];
//    self.sidePanelObj.centerPanel = [[UINavigationController alloc] initWithRootViewController:objHomeVC];
//    [[[UIApplication sharedApplication].delegate window] setRootViewController:self.sidePanelObj];

    _txtPass.text = [_txtPass.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    _txtemail.text = [_txtemail.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    
   if (_txtemail.text.length == 0)
   {
       UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter User Name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
       [alert show];
   }
   else if (_txtemail.text.length == 0)
   {
       UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
       [alert show];
   }
    else
    {
        [self getData];
    }
    

}
-(BOOL)onSuccess:(id)object forTaskCode:(NSString *)taskcode httpObject:(HttpObject *)objHttp{
    [super onSuccess:object forTaskCode:taskcode httpObject:objHttp];
    NSString *str=[NSString stringWithFormat:@"%@",[object valueForKey:@"message"]];
    if (![str isEqualToString:@"success"]) {
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:[object valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:[object valueForKey:@"data"] forKey:@"userdata"];
        [[NSUserDefaults standardUserDefaults] setObject:_txtemail.text forKey:@"email"];

        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        HomeVC *objHomeVC=[storyboard instantiateViewControllerWithIdentifier:@"homeView"];
        self.sidePanelObj = [[JASidePanelController alloc]init];
        self.sidePanelObj.shouldDelegateAutorotateToVisiblePanel = NO;
        self.sidePanelObj.leftPanel = [storyboard instantiateViewControllerWithIdentifier:@"menuView"];
        self.sidePanelObj.centerPanel = [[UINavigationController alloc] initWithRootViewController:objHomeVC];
        [[[UIApplication sharedApplication].delegate window] setRootViewController:self.sidePanelObj];
    }

    return YES;
}

-(void)onFailure:(HttpObject *)paramObject forTaskCode:(NSString *)taskCode{
    [super onFailure:paramObject forTaskCode:taskCode];
    NSLog(@"RT %@", taskCode);
}


-(void)getData{
    //    spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleBounce color:[UIColor whiteColor]];
    //
    //    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.square = YES;
    //    hud.mode = MBProgressHUDModeCustomView;
    //    hud.customView = spinner;
    //    hud.labelText = NSLocalizedString(@"Loading", @"Loading");
    
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:Token forKey:@"token"];
    [dict setObject:_txtemail.text forKey:@"email"];
    [dict setObject:_txtPass.text forKey:@"pwd"];
    HttpObject *http = [[HttpObject alloc] initWithUrl:[NSString stringWithFormat:@"%@Login",BASE_URL] withTaskCode:@"login"];
    http.dicParams = dict;
    http.strMethodType = @"get";
    [self executeTask:http];

    
   /*
    [self startHudAnimating];
    
    NSString *strUrl;
    
        strUrl = [NSString stringWithFormat:@"%@Login?token=%@&email=%@&pwd=%@",BASE_URL,Token,_txtemail.text,_txtPass.text];
    
    
    
    AFHTTPRequestOperationManager *manager=[[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:strUrl]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",nil];
    
    [manager GET:[NSString stringWithFormat:@"%@",@""] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *str=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]];
        if (![str isEqualToString:@"success"]) {
            UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:[responseObject valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:@"data"] forKey:@"userdata"];
            UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
            HomeVC *objHomeVC=[storyboard instantiateViewControllerWithIdentifier:@"homeView"];
            self.sidePanelObj = [[JASidePanelController alloc]init];
            self.sidePanelObj.shouldDelegateAutorotateToVisiblePanel = NO;
            self.sidePanelObj.leftPanel = [storyboard instantiateViewControllerWithIdentifier:@"menuView"];
            self.sidePanelObj.centerPanel = [[UINavigationController alloc] initWithRootViewController:objHomeVC];
            [[[UIApplication sharedApplication].delegate window] setRootViewController:self.sidePanelObj];
        }
        [self stopHudAnimating];
        NSLog(@"%@",  responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        [self stopHudAnimating];
        
    }];
    */
}
@end

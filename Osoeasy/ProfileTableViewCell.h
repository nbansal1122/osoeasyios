//
//  ProfileTableViewCell.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/14/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblValue;
@property (strong, nonatomic) IBOutlet UILabel *lblHeading;

@end

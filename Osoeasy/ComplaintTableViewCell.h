//
//  ComplaintTableViewCell.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/17/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ComplaintTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblcomplaintnO;
@property (strong, nonatomic) IBOutlet UILabel *lblmembername;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblTicket;
@property (strong, nonatomic) IBOutlet UIView *vwtop;

@end

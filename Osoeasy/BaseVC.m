//
//  BaseVC.m
//  Chyc
//
//  Created by Nitin Bansal on 04/03/16.
//  Copyright (c) 2016 Apple. All rights reserved.
//

#import "BaseVC.h"
#import "AppDelegate.h"
@interface BaseVC ()
{
    RTSpinKitView *spinner;
    MBProgressHUD *hud;
    UIView*  blurViewCalled;
    UIView*  blurmainViewCalled;
    
}

@end


@implementation BaseVC


-(void)startHudAnimating{
    [self startHudAnimating:@"Loading..."];
}

-(void)startHudAnimating:(NSString *)msg{
    spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleBounce color:[UIColor whiteColor]];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //   hud.maskView=
    hud.square = YES;
    hud.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.5];
    hud.mode = MBProgressHUDModeCustomView;
    hud.customView = spinner;
    hud.labelText = NSLocalizedString(msg, msg);
    [spinner startAnimating];
    
}

-(void)stopHudAnimating{
    [spinner stopAnimating];
    hud.hidden=YES;
}

- (UIViewController*)classExistsInNavigationController:(Class)class
{
    for (UIViewController *controller in self.navigationController.viewControllers)
    {
        if ([controller isKindOfClass:class])
        {
            return controller;
        }
    }
    return nil;
}
-(void)executeTask:(HttpObject *)http{
    DownloadManager *manager = [[DownloadManager alloc]initWithHttpObject:http Delegate:self andtaskcode:http.strtaskCode];
    [manager startDownload];
}

-(void)executeTask:(NSMutableDictionary *)params forPageUrl:(NSString *)pageUrl{
    HttpObject *http = [[HttpObject alloc] initWithUrl:[NSString stringWithFormat:@"%@%@",BASE_URL,pageUrl] withTaskCode:pageUrl];
    http.dicParams = params;
    http.strMethodType = @"get";
    [self executeTask:http];
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
-(double)getPercentage:(double)totalamount amountpercent:(double)amountPercent
{
    return   (totalamount * amountPercent)/100;
    
}
-(BOOL)onSuccess:(id)object forTaskCode:(NSString*)taskcode httpObject:(HttpObject*)objHttp
{
    [self stopHudAnimating];
    return true;
}
-(void)onFailure:(HttpObject *)paramObject forTaskCode:(NSString*)taskCode
{
    [self stopHudAnimating];

}

-(void)onPreExecute:(id)object forTaskCode:(NSString*)taskcode
{
    [self startHudAnimating];

}
-(NSString*)getuseremail{
    return  [[NSUserDefaults standardUserDefaults ] valueForKey:@"email"];

}
@end

//
//  HomeVC.m
//  Osoeasy
//
//  Created by Apple on 13/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "HomeVC.h"
#import "CalenderCollectionViewCell.h"
#import "ProfileViewController.h"
#import "EasyPollViewController.h"
#import "SocityBillViewController.h"
#import "NoticeTableViewCell.h"
#import "ResidentsVC.h"
@interface HomeVC ()
{
    NSArray *arrayData;
    NSArray *arrayOfColors;
    NSMutableArray *arraydata;
    NSArray *arrcompletedata;

}
@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getdata];
    arraydata =[[NSMutableArray alloc] init];
   //[self getdata];
    arrayData=[NSArray arrayWithObjects:@"Pending Fee",@"Pending Complaints",@"Residents List",@"Announcement",@"Open EasyPoll",@"Pending Amount", nil];
    arrayOfColors=[NSArray arrayWithObjects:[UIColor colorWithRed:217.0/255.0 green:85.0/255.0 blue:80/255.0 alpha:1.0],[UIColor colorWithRed:94.0/255.0 green:183.0/255.0 blue:94/255.0 alpha:1.0],[UIColor colorWithRed:239.0/255.0 green:173.0/255.0 blue:78/255.0 alpha:1.0],[UIColor colorWithRed:66.0/255.0 green:40.0/255.0 blue:102/255.0 alpha:1.0],[UIColor colorWithRed:159.0/255.0 green:78.0/255.0 blue:240/255.0 alpha:1.0],[UIColor colorWithRed:79.0/255.0 green:241.0/255.0 blue:244.0/255.0 alpha:1.0], nil];
     [_objCollectionView registerNib:[UINib nibWithNibName:@"CalenderCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
   _objCollectionView.backgroundColor=[UIColor clearColor];
    _btnResponsiveTimeline.frame=CGRectMake(_btnResponsiveTimeline.frame.origin.x, _objCollectionView.frame.origin.y+_objCollectionView.frame.size.height+10, _btnResponsiveTimeline.frame.size.width, _btnResponsiveTimeline.frame.size.height);
    // Do any additional setup after loading the view.
    
    
    //[AppDelegate creatingCustomButton:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark- collection view data source
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CalenderCollectionViewCell *gridCell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    NSString *strComplete=[NSString stringWithFormat:@"0 \n %@",[arrayData objectAtIndex:indexPath.row]];
    NSAttributedString *attributeString=[[NSAttributedString alloc]initWithString:strComplete];
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithAttributedString:attributeString];
     [mutableAttributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:10] range:NSMakeRange(2, [[arrayData objectAtIndex:indexPath.row] length])];
   [mutableAttributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:20] range:NSMakeRange(0, 1)];
   [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0,[attributeString length])];
    gridCell.labelInstructions.attributedText=mutableAttributedString;
    gridCell.backgroundColor=[arrayOfColors objectAtIndex:indexPath.row];
    gridCell.layer.borderWidth=1.0;
    gridCell.clipsToBounds=YES;
    gridCell.imgView.image = [UIImage imageNamed:@"ic_complain"];
    if (indexPath.row == 2) {
        gridCell.imgView.image = [UIImage imageNamed:@"di_residents"];
        gridCell.labelInstructions.text = [NSString stringWithFormat:@"%@",[[arrcompletedata valueForKeyPath:@"TotalMembers.TotMember"] objectAtIndex:0]];
        gridCell.lbltitle.text=@"Residents List";

    }
    else if (indexPath.row == 0)
    {
        gridCell.lbltitle.text=@"Pending Fee";

        gridCell.imgView.image = [UIImage imageNamed:@"pending_amount"];
        gridCell.labelInstructions.text = [NSString stringWithFormat:@"%@",[[arrcompletedata valueForKeyPath:@"TotalPendingAmount.TotPendingAmount"] objectAtIndex:0]];
    }
    else if (indexPath.row == 1)
    {
       // gridCell.imgView.image = [UIImage imageNamed:@"pending_amount"];
        gridCell.labelInstructions.text = [NSString stringWithFormat:@"%@",[[arrcompletedata valueForKeyPath:@"TotalComplainPending.TotComplainPending"] objectAtIndex:0]];
        gridCell.lbltitle.text=@"Pending Complaints";

    }
    else if (indexPath.row == 3)
    {
        gridCell.imgView.image = [UIImage imageNamed:@"announcement"];
        gridCell.labelInstructions.text = [NSString stringWithFormat:@"%@",[[arrcompletedata valueForKeyPath:@"TotalAnnouncement.TotAnnouncement"] objectAtIndex:0]];
        gridCell.lbltitle.text=@"Annoucement";

    }
    else if (indexPath.row == 4)
    {
        gridCell.imgView.image = [UIImage imageNamed:@"easypoll"];
        gridCell.labelInstructions.text = [NSString stringWithFormat:@"%@",[[arrcompletedata valueForKeyPath:@"TotalPoll.TotPoll"] objectAtIndex:0]];
        gridCell.lbltitle.text=@"Open EasyPoll";

    }
    else if (indexPath.row == 5)
    {
        gridCell.imgView.image = [UIImage imageNamed:@"pending_amount"];
        gridCell.labelInstructions.text = [NSString stringWithFormat:@"%@",[[arrcompletedata valueForKeyPath:@"TotalPendingAmountCount.TotPendingCount"] objectAtIndex:0]];
        gridCell.lbltitle.text=@"Pending Amount";

    }
    if (gridCell.labelInstructions.text.length == 6) {
        gridCell.labelInstructions.text = @"0";
    }
    gridCell.layer.borderColor=[[arrayOfColors objectAtIndex:indexPath.row]CGColor];
    return gridCell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
      return 6;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((CGRectGetWidth(collectionView.frame)-20)/3, 85);
}

#pragma mark - UICollectionViewDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        ProfileViewController *obj=[self.storyboard instantiateViewControllerWithIdentifier:@"profileView"];
       // [self.navigationController pushViewController:obj animated:YES];
    }
    else if (indexPath.row == 1)
    {
        EasyPollViewController *objEasyPollViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"easyPollView"];
        objEasyPollViewController.strType = @"complaint";
        self.sidePanelController.centerPanel = [[UINavigationController alloc]initWithRootViewController:objEasyPollViewController];
    }
    else if (indexPath.row == 2)
    {

    ResidentsVC *objEasyPollViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"ResidentsVC"];
    objEasyPollViewController.strClassType = @"Resident";
    self.sidePanelController.centerPanel = [[UINavigationController alloc]initWithRootViewController:objEasyPollViewController];
    }
    else if (indexPath.row == 3)
    {
        
        EasyPollViewController *objEasyPollViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"easyPollView"];
        objEasyPollViewController.strType = @"Notice";
        self.sidePanelController.centerPanel = [[UINavigationController alloc]initWithRootViewController:objEasyPollViewController];
    }

    else if (indexPath.row==4){
        EasyPollViewController *objEasyPollViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"easyPollView"];
        objEasyPollViewController.strType = @"EasyPoll";
        [self.navigationController pushViewController:objEasyPollViewController animated:YES];
    }
    else if (indexPath.row==5){
        SocityBillViewController *obj=[self.storyboard instantiateViewControllerWithIdentifier:@"societyView"];
       [self.navigationController pushViewController:obj animated:YES];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)doAction:(id)sender{
    AlarmVC *objHomeVC=[self.storyboard instantiateViewControllerWithIdentifier:@"alarmView"];
    [self.navigationController pushViewController:objHomeVC animated:YES];
}


#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arraydata.count ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    
        NoticeTableViewCell*	cell=[tableView dequeueReusableCellWithIdentifier:@"newsCell"];
        
        if (cell==nil) {
            cell=[[[NSBundle mainBundle] loadNibNamed:@"NoticeTableViewCell" owner:self options:nil] objectAtIndex:0];
        }
        
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"nDetails"] isKindOfClass:[NSNull class]]) {
            cell.lblDesc.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"nDetails"];
            
        }
        CGFloat temp =   [self getLabelHeight:_lbltitle];
        cell.lblDesc.frame = CGRectMake(  cell.lblDesc.frame.origin.x,   cell.lblDesc.frame.origin.y,   cell.lblDesc.frame.size.width, temp + 10);
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"nTitle"] isKindOfClass:[NSNull class]]) {
            cell.lblTitle.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"nTitle"];
            
        }
        if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"nType"] isKindOfClass:[NSNull class]]) {
            cell.lblTnoucment.text =[NSString stringWithFormat:@"%@ - %@",[[arraydata objectAtIndex:indexPath.row] valueForKey:@"nType"],[[arraydata objectAtIndex:indexPath.row] valueForKey:@"nDate"]];
            
        }
    cell.iimg.image = [UIImage imageNamed:@"announcement"];
    cell.iimg.contentMode = UIViewContentModeCenter;
    cell.backgroundColor =[UIColor orangeColor];
    cell.lblDesc.hidden = true;
    cell.iimg.frame = CGRectMake(5, 5, 50, 50);
        cell.iimg.layer.cornerRadius = 25;
        cell.iimg.clipsToBounds = true;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        tableView.backgroundColor =[UIColor colorWithRed:196/255.0 green:196/255.0 blue:196/255.0 alpha:1.0];
        cell.backgroundColor = [UIColor colorWithRed:196/255.0 green:196/255.0 blue:196/255.0 alpha:1.0];
        self.view.backgroundColor = [UIColor colorWithRed:196/255.0 green:196/255.0 blue:196/255.0 alpha:1.0];
    cell.vwtop.frame = CGRectMake(cell.vwtop.frame.origin.x, cell.vwtop.frame.origin.y, cell.vwtop.frame.size.width, cell.frame.size.height);
    
        return cell;
    

    
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  }

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
//            if (![[[arraydata objectAtIndex:indexPath.row] valueForKey:@"nDetails"] isKindOfClass:[NSNull class]]) {
//            _lbltitle.text = [[arraydata objectAtIndex:indexPath.row] valueForKey:@"nDetails"];
//            CGFloat temp =   [self getLabelHeight:_lbltitle];
//            return 75 + temp;
//            
//        }
    
        return 65 ;
        
  }
- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}


-(void)getdata{
    //    spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleBounce color:[UIColor whiteColor]];
    //
    //    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.square = YES;
    //    hud.mode = MBProgressHUDModeCustomView;
    //    hud.customView = spinner;
    //    hud.labelText = NSLocalizedString(@"Loading", @"Loading");
    
    [self startHudAnimating];
    
    NSString *strUrl;
        strUrl = [NSString stringWithFormat:@"%@%@?token=%@&email=%@",BASE_URL,DASH_BOARD_URL,Token,[self getuseremail]];
//       strUrl = [NSString stringWithFormat:@"%@%@?token=%@&email=%@",BASE_URL,DASH_BOARD_URL,Token,@"9891826564"];
    
    
    AFHTTPRequestOperationManager *manager=[[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:strUrl]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",nil];
    
    [manager GET:[NSString stringWithFormat:@"%@",@""] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *str=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]];
        if (![str isEqualToString:@"success"]) {
            UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:[responseObject valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
           // [alert show];
        }
        else
        {
            arrcompletedata=[[NSMutableArray alloc] init];
            arraydata = [responseObject valueForKeyPath:@"data.AnnouncementTable"];
            arrcompletedata = [responseObject valueForKeyPath:@"data"];

            [_tblvw reloadData];
            [_objCollectionView reloadData];
        }
        [self stopHudAnimating];
        NSLog(@"%@",  responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView*alert=[[UIAlertView alloc] initWithTitle:@"" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        [self stopHudAnimating];
        
    }];
    
}

@end

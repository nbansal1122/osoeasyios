//
//  InvitationViewController.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/14/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InvitationViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *btnNextACtion;
- (IBAction)btnNextACtion:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtDate;
@property (strong, nonatomic) IBOutlet UITextField *txtdescc;
@property (strong, nonatomic) IBOutlet UITextField *txtTitle;
- (IBAction)nextscreenAction:(id)sender;

@end

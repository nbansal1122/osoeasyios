//
//  SuccessViewController.m
//  Demo
//
//  Created by Martin Prabhu on 5/25/16.
//  Copyright © 2016 test. All rights reserved.
//

#import "SuccessViewController.h"
#import "SocityBillViewController.h"
#import "SucessViewTableViewCell.h"
@interface SuccessViewController ()
{
NSArray*arraydata;
    UITableView *tblview;
}
@end

@implementation SuccessViewController
@synthesize jsondict;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    jsondict=[[NSMutableDictionary alloc]init];
    UIBarButtonItem *btnMenuAction = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow"]style: UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    //    [btnMenuAction setTintColor:kCIAttributeDefault];
    
    self.navigationItem.leftBarButtonItem = btnMenuAction;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ResponseNew:) name:@"JSON_NEW" object:nil];
    arraydata =[NSArray arrayWithObjects:@"Billinng Name:",@"Transaction Id:",@"Description:",@"Amount:",@"Detail of Apartment:",@"Merchant Reference:",@"Payment Id:",@"Mobile Number:",@"Email:", nil];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"JSON_DICT" object:nil userInfo:nil];
}
-(void)backAction
{
    //[self dismissViewControllerAnimated:YES completion:nil];
   
    SocityBillViewController *obj=[self.storyboard instantiateViewControllerWithIdentifier:@"societyView"];
    self.sidePanelController.centerPanel = [[UINavigationController alloc]initWithRootViewController:obj];
    //    SocityBillViewController *obj=[self.storyboard instantiateViewControllerWithIdentifier:@"societyView"];
//    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    self.sidePanelObj = [[JASidePanelController alloc]init];
//    self.sidePanelObj.shouldDelegateAutorotateToVisiblePanel = NO;
//    self.sidePanelObj.leftPanel = [storyboard instantiateViewControllerWithIdentifier:@"menuView"];
//    self.sidePanelObj.centerPanel = [[UINavigationController alloc] initWithRootViewController:obj];
//    [[[UIApplication sharedApplication].delegate window] setRootViewController:self.sidePanelObj];


}
-(void) ResponseNew:(NSNotification *)message
{
    if ([message.name isEqualToString:@"JSON_NEW"])
    {
        NSLog(@"Response = %@",[message object]);
        
        jsondict = [[message object] mutableCopy];
    }
    self.view.hidden = false;

   // [self createtable];

    //[self createPaymentDetailView];
}

-(void)createPaymentDetailView
{
    int YPOS=30;
    
    UILabel * tiltLabel= [[UILabel alloc]initWithFrame:CGRectMake(0, YPOS,self.view.frame.size.width,70)];
    tiltLabel.font = [UIFont fontWithName:@"Helvetica Bold" size:20];
    tiltLabel.backgroundColor = [UIColor clearColor];
    tiltLabel.text=@"Thank you for the payment towards your Socity Bill";
    tiltLabel.textColor = [UIColor blackColor];
    tiltLabel.numberOfLines = 0;
    tiltLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:tiltLabel];
    
   _scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height-140)];
    _scroll.backgroundColor=[UIColor clearColor];
    NSLog(@"scroll width:%f",_scroll.frame.size.width);
    
    int x,gap,height,ypos = 0;
    int font_size;
    int labelWIdth;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        // iPad
        x=20,gap=20,height=40;
        font_size=17;
        labelWIdth=160;
        font_size=21;
        
    }
    else
    {
        // iPhone
        x=40,gap=10,height=30;
        font_size=13;
        labelWIdth=120;
        font_size=14;
    }
    NSArray *arrtemp =[NSArray arrayWithObjects:@"PaymentStatus",@"DeliveryAddress",@"DeliveryCity",@"DeliveryCountry",@"DeliveryPhone",@"DeliveryPostalCode",@"DeliveryState",@"ResponseCode",@"SecureHash",@"IsFlagged",@"DateCreated",@"BillingCity",@"BillingCountry",@"PaymentId",@"Mode",@"BillingPostalCode",@"AccountId", nil];
    [jsondict removeObjectsForKeys:arrtemp];
    NSArray *keyArray=[jsondict allKeys];
    
    for (int i=0;i<[keyArray count];i++)
    {
        NSString * responseString =[jsondict objectForKey:[keyArray objectAtIndex:i]];
        
        UILabel *listLabel = [[UILabel alloc]initWithFrame:CGRectMake(x, ypos, self.view.frame.size.width-x*2, height)];
        
        listLabel.font = [UIFont fontWithName:@"Helvetica" size:font_size];
        
        if ([[keyArray objectAtIndex:i] isEqualToString:@"DeliveryName"]) {
            listLabel.text=[NSString stringWithFormat:@"%@ : %@",@"Socity Name",responseString];
        }
        else   if ([[keyArray objectAtIndex:i] isEqualToString:@"BillingAddress"]) {
            listLabel.frame = CGRectMake(x, ypos, self.view.frame.size.width-x*2, height*2);
            listLabel.numberOfLines = 0;
            listLabel.text=[NSString stringWithFormat:@"%@ : %@",@"Address",responseString];
        }
        else
        {
        listLabel.text=[NSString stringWithFormat:@"%@ : %@",[keyArray objectAtIndex:i],responseString];
        }
        listLabel.backgroundColor = [UIColor clearColor];
        listLabel.textColor = [UIColor blackColor];
        
        listLabel.textAlignment = NSTextAlignmentLeft;
        
        ypos=listLabel.frame.origin.y+listLabel.frame.size.height+gap;
        
        [_scroll addSubview:listLabel];
    }
    //_scroll.backgroundColor =[UIColor orangeColor];
    int btnXPOS=self.view.frame.size.width/2-60;
    // ypos=self.view.frame.size.height-70;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Config" ofType:@"plist"];
    dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    
    float redValues  =  [[[dict valueForKey:@"BUTTON_BG_COLOR"] valueForKey:@"Red"] floatValue];
    float greenValues = [[[dict valueForKey:@"BUTTON_BG_COLOR"] valueForKey:@"Green"]floatValue];
    float blueValues = [[[dict valueForKey:@"BUTTON_BG_COLOR"] valueForKey:@"Blue"]floatValue];
    float alphaValues =[[[dict valueForKey:@"BUTTON_BG_COLOR"] valueForKey:@"alpha"]floatValue];
    
    UIButton *submitBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    submitBtn.frame = CGRectMake(btnXPOS, ypos, 120, 40);
    [submitBtn addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
    submitBtn.backgroundColor = [UIColor colorWithRed:redValues/255.0 green:greenValues/255.0 blue:blueValues/255.0 alpha:alphaValues];
    //[_scroll addSubview:submitBtn];
    
    // Submit Button Label
    UILabel *btnLabel= [[UILabel alloc]initWithFrame:CGRectMake(0, 0, submitBtn.frame.size.width, submitBtn.frame.size.height)];
    btnLabel.text=@"OK";
    btnLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:font_size+3];
    btnLabel.textColor=[UIColor whiteColor];
    btnLabel.textAlignment = NSTextAlignmentCenter;
   // [submitBtn addSubview:btnLabel];
    
    _scroll.contentSize = CGSizeMake(self.view.frame.size.width,ypos+100);
    
    [self.view addSubview:_scroll];
    
}

-(IBAction)submitAction:(id)sender
{
    //    NSString *path = [[NSBundle mainBundle] pathForResource:@"Config" ofType:@"plist"];
    //   dict= [[NSDictionary alloc] initWithContentsOfFile:path];
    
    
    //MERCHANTVIEWCONTROLLER = [dict objectForKey:@"MERCHANTVIEWCONTROLLER"];
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    for (UIViewController *aViewController in allViewControllers)
    {
        NSString *strClass = NSStringFromClass([aViewController class]);
        
        if ([strClass isEqualToString:@"TestKitViewController"])
            
        {
            [self.navigationController popToViewController:aViewController animated:NO];
        }
    }
    
    // ThankYouViewController_two *view2=[[ThankYouViewController_two alloc]init];
    // [self.navigationController pushViewController:view2 animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arraydata.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    SucessViewTableViewCell*	cell=[tableView dequeueReusableCellWithIdentifier:@"newsCell"];
    
    if (cell==nil) {
        cell=[[[NSBundle mainBundle] loadNibNamed:@"SucessViewTableViewCell" owner:self options:nil] objectAtIndex:0];
    }
    cell.lblName.text =[arraydata objectAtIndex:indexPath.row];
    switch (indexPath.row) {
        case 0:
            
            cell.lblValue.text = [NSString stringWithFormat:@"%@",[jsondict objectForKey:@"BillingName"]];
            
            
            
            break;
        case 1:
            
            cell.lblValue.text = [NSString stringWithFormat:@"%@",[jsondict objectForKey:@"TransactionId"]];
            
            
            
            break;
            
        case 2:
            cell.lblValue.text = [NSString stringWithFormat:@"OSOeasy Socity Bill Payment"];
            
            
            break;
            
        case 3:
            cell.lblValue.text = [NSString stringWithFormat:@"%@",[jsondict objectForKey:@"Amount"]];

            
            break;
            
        case 4:
            cell.lblValue.text = [NSString stringWithFormat:@"%@",[jsondict objectForKey:@"BillingAddress"]];
            cell.lblValue.frame = CGRectMake(cell.lblValue.frame.origin.x, 0, self.view.bounds.size.width/2, 70);
            
            break;
            
            
        case 5:
            cell.lblValue.text = [NSString stringWithFormat:@"%@",[jsondict objectForKey:@"MerchantRefNo"]];
            break;
            
            
        case 6:
            cell.lblValue.text = [NSString stringWithFormat:@"%@",[jsondict objectForKey:@"PaymentId"]];
            
            break;
            
            
        case 7:
            cell.lblValue.text = [NSString stringWithFormat:@"%@",[jsondict objectForKey:@"BillingPhone"]];
            break;
            
        case 8:
            cell.lblValue.text = [NSString stringWithFormat:@"%@",[jsondict objectForKey:@"BillingEmail"]];
            
            
            break;
        default:
            break;
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    tableView.backgroundColor =[UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
    cell.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
    self.view.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
    
    tableView.backgroundColor =[UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
    cell.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
    self.view.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
    
    
}

-(void)m_ShadowWithView:(UIView *)view{
    view.layer.masksToBounds = NO;
    // view.layer.cornerRadius = 8; // if you like rounded corners
    view.layer.shadowOffset = CGSizeMake(0,1);
    view.layer.shadowRadius = 1.0;
    view.layer.shadowOpacity = 0.5;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (indexPath.row == 4) {
        return 70;
    }
    return 40;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

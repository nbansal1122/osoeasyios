//
//  VideoCollectionViewCell.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/15/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface VideoCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet AsyncImageView *imgBack;
@property (strong, nonatomic) IBOutlet UIImageView *imgPlay;
@property (strong, nonatomic) IBOutlet UIButton *btnPlay;

@end

//
//  ProfileViewController.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/14/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HeaderTableViewCell.h"
#import "ProfileTableViewCell.h"
#import "AlarmVC.h"
#import "BaseVC.h"
@interface ProfileViewController : BaseVC
@property (strong, nonatomic) IBOutlet UITableView *tblView;
@property (strong,nonatomic) NSString *strType;
@property (strong, nonatomic) IBOutlet UIButton *btnProfile;
- (IBAction)btnPaymentAction:(id)sender;

@end

//
//  AddNewTicketViewController.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/29/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "TicketTableViewCell.h"
@interface AddNewTicketViewController : BaseVC
- (IBAction)backACtion:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnIssue;
- (IBAction)actionIssue:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtbrif;
@property (strong, nonatomic) IBOutlet UITextField *txtDetail;
@property (strong, nonatomic) IBOutlet UIView *vwOption;
- (IBAction)hideoptionView:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)sendTicket:(id)sender;

@end

//
//  AppDelegate.m
//  Osoeasy
//
//  Created by Apple on 12/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "MenuVC.h"
#import "HomeVC.h"
#import "CalanderVC.h"
#import "ResponseViewController.h"
#import "SocityBillViewController.h"
#import "tempViewController.h"
@interface AppDelegate ()
{
  
}
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    HomeVC *objHomeVC=[storyboard instantiateViewControllerWithIdentifier:@"homeView"];
//    self.sidePanelObj = [[JASidePanelController alloc]init];
//    self.sidePanelObj.shouldDelegateAutorotateToVisiblePanel = NO;
//    self.sidePanelObj.leftPanel = [storyboard instantiateViewControllerWithIdentifier:@"menuView"];
//    self.sidePanelObj.centerPanel = [[UINavigationController alloc] initWithRootViewController:objHomeVC];
//    appDelegate.window.rootViewController = self.sidePanelObj;
    
     NSDictionary *attrDict = @{
                               NSFontAttributeName : [UIFont systemFontOfSize:16.0f],
                               NSForegroundColorAttributeName : [UIColor whiteColor]
                               };
    
    [[UINavigationBar appearance] setTitleTextAttributes:attrDict];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"nav"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    NSDictionary *dic =[[NSUserDefaults standardUserDefaults] valueForKey:@"userdata"];
    if (dic) {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        HomeVC *objHomeVC=[storyboard instantiateViewControllerWithIdentifier:@"homeView"];
        self.sidePanelObj = [[JASidePanelController alloc]init];
        self.sidePanelObj.shouldDelegateAutorotateToVisiblePanel = NO;
        self.sidePanelObj.leftPanel = [storyboard instantiateViewControllerWithIdentifier:@"menuView"];
        self.sidePanelObj.centerPanel = [[UINavigationController alloc] initWithRootViewController:objHomeVC];
        [[[UIApplication sharedApplication].delegate window] setRootViewController:self.sidePanelObj];

    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    if (!url)
    {
        return NO;
    }
    
    NSArray *parameterArray = [[url absoluteString] componentsSeparatedByString:@"?"];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:[[NSBundle mainBundle].infoDictionary objectForKey:@"UIMainStoryboardFile"] bundle:[NSBundle mainBundle]];
    
    ResponseViewController *controller = (ResponseViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"tempViewController"];
    
   // SocityBillViewController *obj=[mainStoryboard instantiateViewControllerWithIdentifier:@"societyView"];
//obj.strshowmsg=@"yes";
    controller.transaction_id=[parameterArray objectAtIndex:1];

   self.sidePanelObj.centerPanel = [[UINavigationController alloc]initWithRootViewController:controller];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if (!url)
    {
        return NO;
    }
    
    NSArray *parameterArray = [[url absoluteString] componentsSeparatedByString:@"?"];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:[[NSBundle mainBundle].infoDictionary objectForKey:@"UIMainStoryboardFile"] bundle:[NSBundle mainBundle]];
    ResponseViewController *controller = (ResponseViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"ResponseViewController"];
//    
    controller.transaction_id = [parameterArray objectAtIndex:1];
//
     self.sidePanelObj.centerPanel = [[UINavigationController alloc]initWithRootViewController:controller];
//    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:controller];
//    self.window.rootViewController = navController;

    
////
//    UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
//    self.sidePanelObj.centerPanel = [[UINavigationController alloc] initWithRootViewController:controller];
//    [[[UIApplication sharedApplication].delegate window] setRootViewController:self.sidePanelObj];

    //UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;

  //  [navigationController pushViewController:controller animated:YES];
    
    return YES;
}


+(void)creatingCustomButton:(UIView *)view{
     UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIButton *btn=[[UIButton alloc]initWithFrame:CGRectMake(view.frame.size.width-60, view.frame.size.height-120, 40, 40)];
    [btn setBackgroundColor:[UIColor blackColor]];
    btn.layer.cornerRadius = 20;
    [btn setImage:[UIImage imageNamed:@"alarm"] forState:UIControlStateNormal];
    [btn addTarget:storyboard action:@selector(doAction:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btn];
}

-(void)doAction:(id)sender{
}
@end

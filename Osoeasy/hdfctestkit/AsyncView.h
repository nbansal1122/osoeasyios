//
//  AsyncView.h
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 10/25/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AsyncView : UIView
{
    
    NSURLConnection* connection; //keep a reference to the connection so we can cancel download in dealloc
    NSMutableData* data; //keep reference to the data so we can collect it as it downloads
    //but where is the UIImage reference? We keep it in self.subviews - no need to re-code what we have in the parent class
    UIActivityIndicatorView *activityIndicator;
    
    int iImgContentMode;
}

@property(nonatomic,retain) UIActivityIndicatorView *activityIndicator;

- (void)loadImageFromURL:(NSURL*)url : (int) iImageContentMode;
- (UIImage*) image;
- (void) setLoadingImage;

- (void)loadButtonImageFromURL:(NSURL*)url : (int) iImageContentMode;
@end

//
//  tempViewController.m
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 11/5/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "tempViewController.h"
#import "ResponseViewController.h"
#import "SocityBillViewController.h"
#import "AppDelegate.h"
@interface tempViewController ()

@end

@implementation tempViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:[[NSBundle mainBundle].infoDictionary objectForKey:@"UIMainStoryboardFile"] bundle:[NSBundle mainBundle]];
    
    ResponseViewController *controller = (ResponseViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"ResponseViewController"];
    
    // SocityBillViewController *obj=[mainStoryboard instantiateViewControllerWithIdentifier:@"societyView"];
    //obj.strshowmsg=@"yes";
    controller.transaction_id=_transacid;
    [self.navigationController pushViewController:controller animated:YES];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    if (![_strPage isEqualToString:@"start"]) {

        SocityBillViewController *obj=[self.storyboard instantiateViewControllerWithIdentifier:@"societyView"];
        self.sidePanelController.centerPanel = [[UINavigationController alloc]initWithRootViewController:obj];
    }

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  ResidentsVC.h
//  Osoeasy
//
//  Created by Apple on 17/09/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"
#import "BaseVC.h"
@interface ResidentsVC : BaseVC
@property (weak, nonatomic) IBOutlet UIView *viewSegment;
@property (weak, nonatomic) IBOutlet UIScrollView *objScrollView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITableView *secondTableVIew;
@property (weak, nonatomic) IBOutlet UITableView *thirdTableView;
@property (weak, nonatomic) IBOutlet UITableView *FourthTableView;

@property (strong,nonatomic) NSString *strClassType;
@property (strong,nonatomic) NSString *strType;

@end

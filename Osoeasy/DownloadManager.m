//
//  DownloadManager.m
//  Osoeasy
//
//  Created by Rahul Mehndiratta on 9/25/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "DownloadManager.h"
#import "HttpObject.h"
#import "BaseVC.h"
@implementation DownloadManager
-(instancetype)initWithHttpObject:(HttpObject*) objHttp Delegate:(id <RestDelegate>)restDelegate andtaskcode:(NSString *)taskCode
{
    self = [super init];
    if (self) {
        self.obj  = objHttp;
        self.strTaskCode = taskCode;
        self.delegate = restDelegate;
    }
    return self;
}
-(void)startDownload
{
    [self.delegate onPreExecute:nil forTaskCode:self.strTaskCode];
    NSString*strTask = self.obj.strMethodType;
    if ([[strTask lowercaseString] isEqualToString:@"get"]) {
        [self DownloadGetData];
    }
    else
    {
        [self DownloadPostData];
    }
}
-(void)DownloadGetData
{
   // NSString* strUrl = [self getCompleteURL:self.obj.strUrl andDictParam:self.obj.dicParams];
    AFHTTPRequestOperationManager *manager=[[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:self.obj.strUrl]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",nil];
    
    [manager GET:[NSString stringWithFormat:@"%@",self.obj.strUrl] parameters:self.obj.dicParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self.delegate onSuccess:responseObject forTaskCode:self.strTaskCode httpObject:self.obj];
        NSLog(@"%@",  responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate onFailure:error.localizedDescription forTaskCode:self.strTaskCode];
        
    }];

}
-(void)DownloadPostData
{
    
}

-(NSString*)getCompleteURL:(NSString*)baseUrl andDictParam :(NSDictionary*)dicParam
{
    NSString *strcompleteUrl;
//
    NSArray *arrKeys =[dicParam allKeys];
    for (NSString* key in dicParam) {
        NSString* value = [dicParam objectForKey:key];
        strcompleteUrl = [NSString stringWithFormat:@"%@%@=%@&",strcompleteUrl,key,value];
        // do stuff
    }
     strcompleteUrl = [NSString stringWithFormat:@"%@?%@",baseUrl,strcompleteUrl];
    return strcompleteUrl;
}
@end
